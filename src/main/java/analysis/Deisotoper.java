package analysis;

import annotation.NRPFragmentAnnotation;
import org.expasy.mzjava.core.mol.Atom;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.openscience.cdk.config.Isotopes;
import org.openscience.cdk.formula.IsotopeContainer;
import org.openscience.cdk.formula.IsotopePattern;
import org.openscience.cdk.formula.IsotopePatternGenerator;
import org.openscience.cdk.formula.MolecularFormula;
import org.openscience.cdk.interfaces.IMolecularFormula;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Deisotoper {

    public static void labelIsotopes(List<NRPFragmentAnnotation>[] annotationsSpectra, ConsensusSpectrum experimentalSpectrum, int charge, double mzTolerance, double intTolerance) throws IOException {

        int idxAnnot=0;
        Isotopes isotopesInstance= Isotopes.getInstance();
        IsotopePatternGenerator isotopePatternGenerator= new IsotopePatternGenerator();//10 percent min abundance

        while(idxAnnot<annotationsSpectra.length){
            //we only evaluate those peaks annotated because we can calculate their intensity
            if(annotationsSpectra[idxAnnot]!=null){
                //check if there are possible isotope mzs
                double isotopeMassDiff=1.00335;//1.00287
                int c=charge;
                double expectedMz=experimentalSpectrum.getMz(idxAnnot);
                List<Integer> matchedIdxs= new ArrayList<>();
                double monoisoInt=experimentalSpectrum.getIntensity(idxAnnot);
                while(c>0){
                    double mDiff=isotopeMassDiff/c;
                    int i=1;
                    int idx=1;
                    while (i<4 && (idxAnnot+idx)<experimentalSpectrum.size()){
                        if((experimentalSpectrum.getIntensity(idxAnnot+idx)/monoisoInt)>=0.1){//10 percent min abundance
                            expectedMz=expectedMz+mDiff;
                            double nextMz=experimentalSpectrum.getMz(idxAnnot+idx);
                            if((expectedMz-mzTolerance) <= nextMz && nextMz<= (expectedMz+mzTolerance)){
                                matchedIdxs.add(idx);
                            }else{
                                break;
                            }
                            i++;
                        }
                        idx++;
                    }
                    if (matchedIdxs.size() != 0) {
                        break;
                    }
                    c=c-1;
                }

                //if yes, check that the intensities match
                if (matchedIdxs.size()!= 0){

                    Map<Composition,List<NRPFragmentAnnotation>> compositionIsotopes= new HashMap<>();
                    int highestScore= 0;
                    List<Composition> selectedComp=null;

                    for(NRPFragmentAnnotation nrpFragmentAnnotation: annotationsSpectra[idxAnnot]){
                        if(nrpFragmentAnnotation.getCharge()==c){
                            Composition monoisoComp=nrpFragmentAnnotation.getComposition();
                            if(compositionIsotopes.containsKey(monoisoComp)){
                                compositionIsotopes.get(monoisoComp).add(nrpFragmentAnnotation);
                            }else{
                                IMolecularFormula iMolecularFormula= new MolecularFormula();
                                for (Atom atom:monoisoComp.getAtoms()){
                                    iMolecularFormula.addIsotope(isotopesInstance.getMajorIsotope(atom.toString()),monoisoComp.getCount(atom));
                                }
                                IsotopePattern isotopePattern=isotopePatternGenerator.getIsotopes(iMolecularFormula);
                                int matchesInt=0;
                                List<IsotopeContainer> isotopes=isotopePattern.getIsotopes();
                                int minIsotopes=Math.min(isotopes.size(),(matchedIdxs.size()+1));
                                for (int i =1; i<minIsotopes;i++){
                                    IsotopeContainer isotope=isotopes.get(i);
                                    double spectrumInt= experimentalSpectrum.getIntensity(idxAnnot+matchedIdxs.get(i-1));
                                    double expectedInt=isotope.getIntensity()*experimentalSpectrum.getIntensity(idxAnnot);
                                    if((expectedInt-intTolerance) <= spectrumInt && spectrumInt<= (expectedInt+intTolerance)){
                                        matchesInt++;
                                    }else{
                                        break;
                                    }
                                }

                                if(matchesInt >0 && matchesInt>=highestScore){
                                    List<NRPFragmentAnnotation> nrpFragmentAnnotations= new ArrayList<>();
                                    nrpFragmentAnnotations.add(nrpFragmentAnnotation);
                                    compositionIsotopes.put(nrpFragmentAnnotation.getComposition(),nrpFragmentAnnotations);

                                    if(matchesInt==highestScore){
                                        selectedComp.add(nrpFragmentAnnotation.getComposition());
                                    }else{
                                        selectedComp=new ArrayList<>();
                                        selectedComp.add(nrpFragmentAnnotation.getComposition());
                                        highestScore= matchesInt;
                                    }
                                }
                            }
                        }
                    }
                    if(highestScore!=0){
                        List<NRPFragmentAnnotation> finalIsotopes;
                        if(selectedComp.size()==1){
                            finalIsotopes=compositionIsotopes.get(selectedComp.get(0));
                        }else{
                            finalIsotopes= new ArrayList<>();
                            for(Composition composition:selectedComp){
                                finalIsotopes.addAll(compositionIsotopes.get(composition));
                            }
                        }
                        for (int i =1; i<=highestScore;i++){
                            List<NRPFragmentAnnotation> nrpFragmentAnnotations=new ArrayList<>();
                            nrpFragmentAnnotations.add(new NRPFragmentAnnotation(c,true));

                            annotationsSpectra[idxAnnot+matchedIdxs.get(i-1)]= nrpFragmentAnnotations;
                        }

                        annotationsSpectra[idxAnnot]=finalIsotopes;
                        idxAnnot=idxAnnot+matchedIdxs.get(highestScore-1);

                    }
                }
            }

            idxAnnot++;
        }

    }
}
