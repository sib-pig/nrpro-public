package analysis;

import annotation.NRPFragmentAnnotation;

import java.util.List;

public class MatchResult {
    private List<NRPFragmentAnnotation>[] annotationsSpectra;
    private boolean[] scoredPeaks;

    MatchResult(List<NRPFragmentAnnotation>[] annotationsSpectra, boolean[] scoredPeaks) {
        this.annotationsSpectra = annotationsSpectra;
        this.scoredPeaks = scoredPeaks;
    }

    public List<NRPFragmentAnnotation>[] getAnnotationsSpectra() {
        return annotationsSpectra;
    }

    public boolean[] getScoredPeaks() {
        return scoredPeaks;
    }
}
