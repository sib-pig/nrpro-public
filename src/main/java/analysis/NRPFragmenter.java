package analysis;
import annotation.Annotation;
import annotation.Annotator;
import bond.MSFragmentPosition;
import graph.*;
import interfaces.Monomeric;
import ion.DissociationTechnique;
import ion.IonType;
import ion.IonizationStore;
import ion.Terminus;
import molecules.bond.BondType;
import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.alg.connectivity.ConnectivityInspector;
import org.jgrapht.graph.SimpleDirectedGraph;
import misc.CompositionsJoiner;

import java.io.IOException;
import java.util.*;

/**
 * Created by ericart on 04.03.2016.
 */
//the simple directed graph has a matching that is not exact (if two fragment are equal in two different part of the molecule is the same)
//the list nrpfragment differenciates all fragments, two fragments that are equal but in different positions are differenciated
public class NRPFragmenter {

    private Annotator annotator;
    private double minMass;
    private int depthLimit;
    private int graphIdx =0;
    private int nrpFragIdx=0;
    private boolean isDecoy;
    private SimpleDirectedGraph<NRPFragment, MonGraph.Loss> tree;
    private Map<SimpleDirectedGraph<MSFragment, MSBond>,List<NRPFragment>> repeatedFrags;
    private Set<BondType> targetBonds;


    public NRPFragmenter(DissociationTechnique dissociationTechnique, int depthLimit){
        IonizationStore ionizationStore= IonizationStore.getInstance();
        this.targetBonds=ionizationStore.getBondsIonitzation(dissociationTechnique);
        this.depthLimit=depthLimit;
        this.repeatedFrags = new HashMap<>(0);
    }

    public NRPFragmentTree createFragmentationTree(Monomeric nrp, double minMass, boolean isDecoy) throws IOException {
        this.isDecoy=isDecoy;
        this.minMass=minMass-2;
        SimpleDirectedGraph <MSFragment,MSBond> simpleGraph = nrp.getMonomericGraph();
        NRPFragment nrpFragment;
        Composition composition = Composition.parseComposition(nrp.getFormula());
        int depth=0;
        if(!isDecoy){
            this.annotator= new Annotator(nrp.getMonomericGraph().vertexSet(),nrp.getSize());
            Annotation annotation= annotator.annotateFragment(simpleGraph.vertexSet());
            annotation.setIonAnnot("PRE"+annotation.getCondensed());
            nrpFragment= new NRPFragment(simpleGraph, composition, depth,annotation);
            depth++;
        }else{
            nrpFragment= new NRPFragment(simpleGraph, composition,depth,new Annotation("","",null));
            depth++;
        }
        nrpFragment.setGraphId(new MonGraphId(graphIdx));
        nrpFragment.setNrpFragmentId(new NRPFragmentId(nrpFragIdx));
        NRPFragment root = nrpFragment;
        this.tree = new SimpleDirectedGraph<>(MonGraph.Loss.class);
        this.tree.addVertex(nrpFragment);
        List<NRPFragment> newFragmentList= new ArrayList<>();
        newFragmentList.add(nrpFragment);
        this.repeatedFrags.put(simpleGraph,newFragmentList);
        graphIdx++;
        nrpFragIdx++;
        List<NRPFragment> parentFragments = new ArrayList<>();
        parentFragments.add(nrpFragment);

        while (depth<this.depthLimit){
            List<NRPFragment> children =generateChildren(parentFragments);
            if(children.isEmpty())
                break;
            parentFragments=children;
            depth++;
        }
        //System.out.println("total unique frags: "+this.repeatedFrags.size());
        //System.out.println(this.tree.vertexSet().size());
        return new NRPFragmentTree(nrp,root,this.tree);
    }

    private List<NRPFragment> generateChildren(List<NRPFragment> parentFragments) throws IOException {
        List<NRPFragment> childrenFragments= new ArrayList<>();
        for(NRPFragment nrpFragment: parentFragments){
            childrenFragments.addAll(cutFragment(nrpFragment));
        }

        return childrenFragments;
    }
    //when there is a single parent we use this method
    private List<NRPFragment> cutFragment(NRPFragment parentGraph) throws IOException {
        //this list is created to store the children graphs created, because they will be the parents for the next round
        //unless they contain a single monomer (cant be split anymore).
        List<NRPFragment> children= new ArrayList<>();
        for (MSBond targetbond : parentGraph.getAllBond()){
            BondType bondType=targetbond.getBondTypes().get(0);
            int[] monIdxs=targetbond.getMonIdxs().get(0);

            if (this.targetBonds.contains(bondType)){

                SimpleDirectedGraph<MSFragment,MSBond> simpleGraph = parentGraph.cloneSimpleGraph();
                simpleGraph.removeEdge(targetbond);
                MSFragment monomerSource =targetbond.getSource();
                MSFragment monomerTarget =targetbond.getTarget();
                Map<MSFragment,List<Terminus>> oldRearrangements=parentGraph.getTerminals();
                ConnectivityInspector<MSFragment, MSBond> connectivityInspector = new ConnectivityInspector<>(simpleGraph);
                List<NRPFragment> childrenList = new ArrayList<>();//map nrpfragment + properties (target, source and bondcut)

                //we check if the removed bond generates 1 or 2 fragments
                if (connectivityInspector.isConnected()){

                    //NRPFragment childGraph= new NRPFragment(simpleGraph, parentGraph.getComposition(),root.getPrecursorId(),parentGraph.getFragmentationStage()+1);

                    NRPFragment.Builder nrpFragBuilder = new NRPFragment.Builder(simpleGraph,parentGraph.getComposition(),parentGraph.getDepth());
                    if(!isDecoy){
                        nrpFragBuilder.setAnnotation(annotator.annotateFragment(simpleGraph.vertexSet()));
                    }
                    nrpFragBuilder.addNewRearrangement(monomerSource,bondType, MSFragmentPosition.SOURCE,monIdxs);
                    nrpFragBuilder.addNewRearrangement(monomerTarget,bondType,MSFragmentPosition.TARGET,monIdxs);

                    for (MSFragment m :oldRearrangements.keySet()){
                        for (Terminus terminus : oldRearrangements.get(m) ){
                            nrpFragBuilder.addOldRearrangement(m, terminus);
                        }
                    }

                    NRPFragment childGraph= nrpFragBuilder.build();
                    childrenList.add(childGraph);

                }else{

                    List<Set<MSFragment>> connectedSets = connectivityInspector.connectedSets();
                    for (Set<MSFragment> nodes : connectedSets){
                        SimpleDirectedGraph<MSFragment,MSBond> simpleGraph2 = parentGraph.cloneSimpleGraph();
                        simpleGraph2.removeAllEdges(parentGraph.getBondsSubset(nodes));
                        simpleGraph2.removeAllVertices(nodes);
                        Composition originalComp=CompositionsJoiner.joinCompositions(simpleGraph2.vertexSet());
                        if(originalComp.getMolecularMass()>=this.minMass){
                            NRPFragment.Builder nrpFragBuilder = new NRPFragment.Builder(simpleGraph2,originalComp, parentGraph.getDepth()+1);

                            if (simpleGraph2.vertexSet().contains(monomerSource)){
                                nrpFragBuilder.addNewRearrangement(monomerSource,bondType,MSFragmentPosition.SOURCE,monIdxs);
                                if(!isDecoy){
                                    nrpFragBuilder.setAnnotation(annotator.annotateFragment(simpleGraph2.vertexSet()));
                                }

                            }
                            if (simpleGraph2.vertexSet().contains(monomerTarget)){
                                nrpFragBuilder.addNewRearrangement(monomerTarget,bondType,MSFragmentPosition.TARGET,monIdxs);
                                if(!isDecoy){
                                    nrpFragBuilder.setAnnotation(annotator.annotateFragment(simpleGraph2.vertexSet()));
                                }
                            }

                            for (MSFragment m : oldRearrangements.keySet()){
                                if (simpleGraph2.vertexSet().contains(m)){
                                    for (Terminus terminus : oldRearrangements.get(m) ) {
                                        nrpFragBuilder.addOldRearrangement(m, terminus);
                                    }
                                }
                            }

                            NRPFragment childGraph= nrpFragBuilder.build();
                            //NRPFragment childGraph= new NRPFragment(simpleGraph2, CompositionsJoiner.joinCompositions(simpleGraph2.vertexSet()),root.getPrecursorId(),parentGraph.getFragmentationStage()+1);
                            childrenList.add(childGraph);
                            //System.out.println(childGraph.getAnnotation());
                            //System.out.println();
                        }
                    }
                }

                for(NRPFragment childNrpFragment : childrenList) {
                    //do not add x ions for now
                    List<Terminus> breakages=childNrpFragment.getBreakage();
                    boolean hasA=false;
                    boolean hasX=false;
                    boolean hasC=false;
                    boolean hasZ=false;
                    boolean rareIon=false;
                    for(Terminus terminus: breakages){
                        if(terminus.getIonType().equals(IonType.A)){
                            if(hasX){
                                rareIon=true;
                                break;
                            }
                            hasA=true;
                        }
                        if(terminus.getIonType().equals(IonType.X)){
                            if(hasA){
                                rareIon=true;
                                break;
                            }
                            hasX=true;
                        }
                        if(terminus.getIonType().equals(IonType.C)){
                            if(hasZ){
                                rareIon=true;
                                break;
                            }
                            hasC=true;
                        }
                        if(terminus.getIonType().equals(IonType.Z)){
                            if(hasC){
                                rareIon=true;
                                break;
                            }
                            hasZ=true;
                        }
                    }
                    if(rareIon){
                        continue;
                    }
                    SimpleDirectedGraph<MSFragment, MSBond> simpleDirectedGraph= childNrpFragment.getSimpleGraph();
                    List<NRPFragment> sameFragments=repeatedFrags.get(simpleDirectedGraph);
                    if(sameFragments!=null){
                        MonGraphId id=sameFragments.get(0).getGraphId();
                        childNrpFragment.setGraphId(id);
                        sameFragments.add(childNrpFragment);
                    }else{
                        childNrpFragment.setGraphId(new MonGraphId(graphIdx));
                        List<NRPFragment> newFragmentList= new ArrayList<>();
                        newFragmentList.add(childNrpFragment);
                        this.repeatedFrags.put(simpleDirectedGraph,newFragmentList);
                        graphIdx++;
                    }

                    childNrpFragment.setNrpFragmentId(new NRPFragmentId(nrpFragIdx));
                    this.tree.addVertex(childNrpFragment);
                    this.tree.addEdge(parentGraph, childNrpFragment, new MonGraph.Loss(parentGraph, childNrpFragment,targetbond));
                    nrpFragIdx++;
                    if (!childNrpFragment.isEndMolecule()) {
                        children.add(childNrpFragment);
                    }
                }
            }
        }

        return children;
    }

}
