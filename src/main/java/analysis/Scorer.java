package analysis;

import annotation.NRPFragmentAnnotation;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import java.util.Comparator;
import java.util.List;

public class Scorer {
    public static Scores getDotProduct(List<NRPFragmentAnnotation>[] annotationsSpectra, ConsensusSpectrum experimentalSpectrum, boolean[] scoredPeaks, boolean isDecoy){
        double dotProduct=0;
        int numAnnotPeaks=0;
        int numScoredPeaks=0;
        int numIsotopes=0;
        int numPeaksHigher10I=0;
        for (int idx=0;idx<=annotationsSpectra.length-1;idx++){
            List<NRPFragmentAnnotation> annotations=annotationsSpectra[idx];

            if(annotations==null)
                continue;

            if(annotations.size()==1){
                NRPFragmentAnnotation singleAnnot=annotations.get(0);
                if(!singleAnnot.isIsotope()  && singleAnnot.getAnnotScore()<2 && scoredPeaks[idx]){
                    double intPeak=experimentalSpectrum.getIntensity(idx);
                    dotProduct=dotProduct+intPeak;
                    numScoredPeaks++;
                    if(intPeak>5) {
                        numPeaksHigher10I++;
                    }
                }
                if(!isDecoy){
                    experimentalSpectrum.addAnnotation(idx,singleAnnot);
                    numAnnotPeaks++;
                    if(singleAnnot.isIsotope()){
                        numIsotopes++;
                    }
                }

            }else{
                annotations.sort(Comparator.comparing(a -> a.getAnnotScore()));
                int i=0;
                for(NRPFragmentAnnotation nrpFragmentAnnotation:annotations){
                    if(nrpFragmentAnnotation.getAnnotScore()<2){
                        if(scoredPeaks[idx]){
                            if(i!=0){
                                NRPFragmentAnnotation newFirst =annotations.get(i);
                                annotations.remove(i);
                                annotations.add(0,newFirst);
                            }
                            double intPeak=experimentalSpectrum.getIntensity(idx);
                            dotProduct=dotProduct+intPeak;
                            numScoredPeaks++;
                            if(intPeak>5) {
                                numPeaksHigher10I++;
                            }

                            break;
                        }
                    }else{
                        break;
                    }
                    i++;
                }
                if(annotations.size()>4){
                   annotations= annotations.subList(0, 3);
                }

                experimentalSpectrum.addAnnotations(idx,annotations);
                numAnnotPeaks++;
            }
        }

        return new Scores(dotProduct,numAnnotPeaks,numScoredPeaks,numIsotopes,numPeaksHigher10I);
    }
}
