package analysis;

public class Scores {
    private double dotProduct=0;
    private int numAnnotPeaks=0;
    private int numScoredPeaks=0;
    private int numIsotopes=0;
    private int peaksHigher10=0;

    public Scores(double dotProduct, int numAnnotPeaks, int numScoredPeaks, int numIsotopes,int peaksHigher10) {
        this.dotProduct = dotProduct;
        this.numAnnotPeaks = numAnnotPeaks;
        this.numScoredPeaks = numScoredPeaks;
        this.numIsotopes=numIsotopes;
        this.peaksHigher10=peaksHigher10;
    }

    public double getDotProduct() {
        return dotProduct;
    }

    public int getNumAnnotPeaks() {
        return numAnnotPeaks;
    }

    public int getNumScoredPeaks() {
        return numScoredPeaks;
    }

    public int getNumIsotopes() {
        return numIsotopes;
    }

    public int getPeaksHigher10() {
        return peaksHigher10;
    }
}
