package analysis;

import interfaces.Monomeric;
import ion.DissociationTechnique;
import graph.NRPFragmentTree;
import ion.Tolerance;
import misc.Adduct;
import misc.NeutralLoss;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

public class SpectrumMatcher {

    private Tolerance ionTolerance;
    private List<NeutralLoss> neutralLosses;
    private List<Adduct> adducts;
    private DissociationTechnique dissociationTechnique;
    private Adduct protonAdduct;
    private boolean deisotoping;


    public SpectrumMatcher(Tolerance tolerance, List<NeutralLoss> neutralLosses, List<Adduct> adducts , DissociationTechnique dissociationTechnique, Adduct protonAdduct,boolean deisotoping) {

        this.ionTolerance =tolerance;
        this.deisotoping= deisotoping;
        this.neutralLosses=neutralLosses;
        this.adducts=adducts;
        this.dissociationTechnique=dissociationTechnique;
        this.protonAdduct=protonAdduct;
    }

    public Scores getSpectralMatch(Monomeric nrProCompound, ConsensusSpectrum consensusSpectrum, int charge,boolean isDecoy) throws URISyntaxException, IOException {

        NRPFragmenter nrpFragmenter=null;
        if(nrProCompound.getSize()>19){//it would be better to check wether they are linear or not
            nrpFragmenter= new NRPFragmenter(dissociationTechnique,3);
        }else{
            nrpFragmenter= new NRPFragmenter(dissociationTechnique,4);
        }
        NRPFragmentTree simpleDirectedGraph= nrpFragmenter.createFragmentationTree(nrProCompound,consensusSpectrum.getMz(0),false);
        TreeAnnotator treeAnnotator = new TreeAnnotator(neutralLosses,adducts,charge, this.protonAdduct);
        treeAnnotator.addAnnotations(simpleDirectedGraph);
        SpectrumMatcherHelper spectrumMatcherHelper = new SpectrumMatcherHelper(ionTolerance);
        MatchResult matchResult= spectrumMatcherHelper.match(simpleDirectedGraph,consensusSpectrum);


        if(deisotoping && !isDecoy){
           Deisotoper.labelIsotopes(matchResult.getAnnotationsSpectra(),consensusSpectrum,charge,0.001,5);
        }
        return Scorer.getDotProduct(matchResult.getAnnotationsSpectra(),consensusSpectrum,matchResult.getScoredPeaks(),isDecoy);


    }

    public Tolerance getIonTolerance() {
        return ionTolerance;
    }

    public List<NeutralLoss> getNeutralLosses() {
        return neutralLosses;
    }

    public List<Adduct> getAdducts() {
        return adducts;
    }

    public DissociationTechnique getDissociationTechnique() {
        return dissociationTechnique;
    }

    public Adduct getProtonAdduct() {
        return protonAdduct;
    }

    public boolean isDeisotoping() {
        return deisotoping;
    }
}
