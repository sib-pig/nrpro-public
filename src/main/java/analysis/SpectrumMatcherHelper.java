package analysis;

import annotation.NRPFragmentAnnotation;
import graph.*;
import ion.Tolerance;
import misc.Ppm2DaConverter;

import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.jgrapht.traverse.BreadthFirstIterator;
import java.util.*;

public class SpectrumMatcherHelper {

    private double [] experimentalMzs;
    private boolean[] scoredPeaks;
    private double originalTolerance;
    private boolean isInPpm=false;
    public SpectrumMatcherHelper(Tolerance originaltolerance) {
        this.originalTolerance=originaltolerance.getTolerance();
        if(originaltolerance.getUnitTolerance()== Tolerance.UnitTolerance.PPM){
            isInPpm=true;
        }
    }

    public MatchResult match(NRPFragmentTree annotatedTree, ConsensusSpectrum experimentalSpectrum){
        this.experimentalMzs= experimentalSpectrum.getMzs(new double[experimentalSpectrum.size()]);
        Set<NRPFragmentId> matchedFrags= new HashSet<>();
        Map<MonGraphId,PeakScoringState> alreadyAnnotated= new HashMap<>();
        List<NRPFragmentAnnotation>[] annotationsSpectra= new List[experimentalSpectrum.size()];
        BreadthFirstIterator<NRPFragment, MonGraph.Loss> treeIterator=annotatedTree.getBreathFirstIterator();
        this.scoredPeaks = new boolean[experimentalSpectrum.size()];
        boolean isParentScored=true;
        double tolerance=this.originalTolerance;
        while (treeIterator.hasNext()){
            NRPFragment nrpFrag=treeIterator.next();
            NRPFragment parent=treeIterator.getParent(nrpFrag);
            //System.out.println(nrpFrag.getDepth());
            if(parent !=null && matchedFrags.contains(parent.getNrpFragmentId())){
                isParentScored=true;
            }
            if(alreadyAnnotated.containsKey(nrpFrag.getGraphId())){
                PeakScoringState peakScoringState=alreadyAnnotated.get(nrpFrag.getGraphId());
                if(isParentScored && peakScoringState.hasScorableMatch ){
                    matchedFrags.add(nrpFrag.getNrpFragmentId());
                    if (!peakScoringState.areScored){
                        peakScoringState.setAreScored(true);
                    }
                }
            }else{
                boolean fragScored=false;
                boolean hasScorableMatch=false;
                List<Integer> annotatedIdxs= new ArrayList<>();

                for (double theoreticalMz:nrpFrag.getMzs()){
                    if(this.isInPpm){
                        tolerance= Ppm2DaConverter.convert(this.originalTolerance,theoreticalMz);
                    }
                    double minMz=theoreticalMz-tolerance;
                    double maxMz=theoreticalMz+tolerance;
                    int matchedExpIdx= binarySearch(minMz,maxMz);
                    if(matchedExpIdx>=0){//matches
                        NRPFragmentAnnotation fragmentAnnotation = nrpFrag.getNRPFragmentAnnotation(theoreticalMz);
                        if(annotationsSpectra[matchedExpIdx]!=null){
                            annotationsSpectra[matchedExpIdx].add(fragmentAnnotation);
                        }else{
                            List<NRPFragmentAnnotation> annotations= new ArrayList<>();
                            annotations.add(fragmentAnnotation);
                            annotationsSpectra[matchedExpIdx]=annotations;
                        }

                        if(!fragScored && fragmentAnnotation.getAnnotScore()<2){// has no more than 1 typical NL
                            hasScorableMatch=true;
                            if(isParentScored || nrpFrag.getDepth()<2){//|| hasZeroMatches
                                matchedFrags.add(nrpFrag.getNrpFragmentId());
                                fragScored=true;
                            }
                        }
                        annotatedIdxs.add(matchedExpIdx);
                    }
                }
                alreadyAnnotated.put(nrpFrag.getGraphId(),new PeakScoringState(fragScored,annotatedIdxs,hasScorableMatch));
            }
            isParentScored=false;
        }

        return new MatchResult(annotationsSpectra,scoredPeaks);
    }

    public class PeakScoringState{
        private boolean areScored;
        private List<Integer> peaks;
        private boolean hasScorableMatch;

        public PeakScoringState(boolean areScored, List<Integer> peaks, boolean hasScorableMatch) {
            this.peaks = peaks;
            this.hasScorableMatch=hasScorableMatch;
            setAreScored(areScored);
        }

        public void setAreScored(boolean areScored) {
            this.areScored = areScored;
            if(areScored){
                for (int i :this.peaks){
                    scoredPeaks[i]=true;
                }
            }
        }
    }

    private  int binarySearch(double min, double max) {
        int low = 0;
        int high = this.experimentalMzs.length - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            double midVal = this.experimentalMzs[mid];

            if (midVal < min && midVal < max)
                low = mid + 1;  // Neither val is NaN, thisVal is smaller
            else if (midVal > min && midVal > max)
                high = mid - 1; // Neither val is NaN, thisVal is larger
            else {
                return mid;             // Key found
            }
        }
        return -(low + 1);  // key not found.
    }

}
