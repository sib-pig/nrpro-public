package analysis;

import annotation.NRPFragmentAnnotation;
import graph.NRPFragment;
import misc.Adduct;
import misc.NeutralLoss;
import molecules.monomer.MSFragment;
import ion.IonType;
import graph.NRPFragmentTree;
import ion.Terminus;
import org.expasy.mzjava.core.mol.AtomicSymbol;
import org.expasy.mzjava.core.mol.Composition;
import org.expasy.mzjava.core.mol.PeriodicTable;


import java.util.*;

public class TreeAnnotator {

    Map<NeutralLoss,Composition> neutralLosses = new HashMap<>();
    Map<Adduct,Composition> adducts = new HashMap<>();
    Adduct protonAdduct;
    boolean useMobileProtModel=true;
    int charge;

    public TreeAnnotator(List <NeutralLoss> neutralLosses, List <Adduct> adducts, int charge, Adduct protonAdduct) {

        for(NeutralLoss neutralLoss: neutralLosses){
            this.neutralLosses.put(neutralLoss,Composition.parseComposition(neutralLoss.toString()));
        }
        for(Adduct adduct: adducts){
            this.adducts.put(adduct,Composition.parseComposition(adduct.toString()));
        }
        this.charge=charge;
        this.protonAdduct=protonAdduct;
        if(!protonAdduct.equals(Adduct.H)){
            useMobileProtModel=false;
        }
    }

    public void addAnnotations(NRPFragmentTree nrpFragmentTree){

        for(NRPFragment nrpFragment : nrpFragmentTree.getFragments()){

            /*if( nrpFragment.getMolecularMass()<175 && nrpFragment.getAnnotation().getFull().contains("Phe") ){
                System.out.println(nrpFragment.getAnnotation().getIonAnnot());
                System.out.println(nrpFragment.getAnnotation().getFull());
                System.out.println(nrpFragment.getMolecularMass());
            }

            if (nrpFragment.getAnnotation().getIonAnnot().contains("Y(E)")  ) {
                if ( nrpFragment.getMolecularMass()>940 && nrpFragment.getMolecularMass()<950) {
                    System.out.println();
                }
            }
            if ( nrpFragment.getAnnotation().getIonAnnot().contains("Z(E)")) {
                if ( nrpFragment.getMolecularMass()>920 && nrpFragment.getMolecularMass()<935) {
                    System.out.println();
                }
            }*/

            boolean rootNotFound=true;
            //Map<MSFragment, List<Terminus>> listTerminals = nrpFragment.getTerminals();
            int numberHydrogens=0;
            int protons=0;
            boolean addProton=true;
            if(useMobileProtModel){
                for(Terminus terminus:nrpFragment.getBreakage()){
                    if(terminus.ishGain()){
                        numberHydrogens++;
                    }
                    if(!terminus.isProtonAcceptor()){
                        addProton=false;
                    }
                }
            }

            if(addProton){
                protons=1;
            }
            double fragMass=nrpFragment.getMolecularMass();

            List<Integer> numHydrArray= new ArrayList<>();
            numHydrArray.add(numberHydrogens);

            int score= 0;
            int h=1;
            for(Terminus terminus:nrpFragment.getBreakage()){
                if(terminus.getIonType().equals(IonType.A)){
                    score++;
                }
                if(terminus.getIonType().equals(IonType.X)){
                    score=score+2;
                }
                if(terminus.getIonType().equals(IonType.Z)){
                    //SHOULD NOT ACCEPT TWO Z IONS!
                    score=score+2;
                    if(useMobileProtModel){
                        numHydrArray.add(numberHydrogens+h);
                        numHydrArray.add(numberHydrogens-h);
                        h++;
                    }
                }
            }

            for(int numHydr: numHydrArray){
                for(int c=1;c<=charge;c++){
                    protons=protons+(c-1);
                    double hydroMass=numHydr*PeriodicTable.H_MASS;
                    double protonsMass=protons*protonAdduct.getModMass();
                    double cMass=c * PeriodicTable.ELECTRON_MASS;
                    double mz =fragMass+hydroMass+protonsMass-cMass;
                    double mass= fragMass+hydroMass;
                    int totalHydrogens=numHydr+protons;
                    Composition composition;
                    Composition hydroComp =new Composition.Builder(AtomicSymbol.valueOf(protonAdduct.toString()),Math.abs(totalHydrogens)).build();

                    if(totalHydrogens>=0){
                        composition= new Composition(nrpFragment.getComposition(),hydroComp);
                    }else{
                        composition= Composition.subtractCompositions(nrpFragment.getComposition(),hydroComp);//sure?
                    }

                    if(rootNotFound && nrpFragmentTree.isRoot(nrpFragment)){
                        rootNotFound=false;
                    }

                    nrpFragment.addResultingFragment(mz/c,new NRPFragmentAnnotation(mz/c,mass,c,nrpFragment,null,composition,score));

                    for(NeutralLoss nl: this.neutralLosses.keySet()){
                        double mzNL =(mz-nl.getModMass())/c;
                        try {
                            Composition composition2= Composition.subtractCompositions(composition,this.neutralLosses.get(nl));
                            if(nl.equals(NeutralLoss.H2O) || (nl.equals(NeutralLoss.NH3))){
                                nrpFragment.addResultingFragment(mzNL,new NRPFragmentAnnotation(mzNL,mass,c,nrpFragment,"-"+nl.toString(),composition2,score+1));
                            }else{
                                nrpFragment.addResultingFragment(mzNL,new NRPFragmentAnnotation(mzNL,mass,c,nrpFragment,"-"+nl.toString(),composition2,score+2));
                            }

                        }catch (IllegalStateException illegalStateException){

                        }
                    }

                    for(Adduct adduct: this.adducts.keySet()){
                        double mzNL =(mz+adduct.getModMass())/c;
                        try {
                            Composition composition2= new Composition(composition,this.adducts.get(adduct));
                            if(adduct.equals(Adduct.H2O)){
                                nrpFragment.addResultingFragment(mzNL,new NRPFragmentAnnotation(mzNL,mass,c,nrpFragment,"+"+adduct.toString(),composition2,score+1));
                            }else{
                                nrpFragment.addResultingFragment(mzNL,new NRPFragmentAnnotation(mzNL,mass,c,nrpFragment,"+"+adduct.toString(),composition2,score+2));
                            }
                        }catch (IllegalStateException illegalStateException){
                        }
                    }
                }
            }
        }
    }
}
