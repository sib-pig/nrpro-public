package annotation;

public class Annotation {
    private String condensed;
    private String full;
    private String ionAnnot;
    private int[] monomerIdxs;//needed to highlight them in the interface

    public Annotation() {
    }

    public Annotation(String condensed, String full, int[] monomerIdxs) {
        this.condensed = condensed;
        this.full = full;
        this.monomerIdxs= monomerIdxs;
    }

    public String getCondensed() {
        return condensed;
    }

    public String getFull() {
        return full;
    }

    public String getIonAnnot() {
        return ionAnnot;
    }


    public int[] getMonomerIdxs() {
        return monomerIdxs;
    }

    public void setIonAnnot(String ionAnnot) {
        this.ionAnnot = ionAnnot;
    }

    @Override
    public String toString() {
        return "Annotation{" +
                "condensed='" + condensed + '\'' +
                ", full='" + full + '\'' +
                ", ionAnnot='" + ionAnnot + '\'' +
                '}';
    }
}
