package annotation;

import molecules.monomer.MSFragment;
import molecules.monomer.MonomerAnnotation;
import org.expasy.mzjava.core.mol.Composition;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Annotator {
    Map<String,Composition> fragCompositions= new HashMap<>();
    int monSize;

    public Annotator(Set<MSFragment> fragmentSet, int monSize) {
        for (MSFragment msFragment :fragmentSet){
            List<MonomerAnnotation> annotationList=msFragment.getMonomerAnnotations();
            for(int i=0;i<annotationList.size();i++){
                fragCompositions.put(msFragment.getIndex()+"."+i,Composition.parseComposition(annotationList.get(i).getFormula()));
            }
        }
        this.monSize=monSize;
    }

    public Annotation annotateFragment(Set<MSFragment> fragmentSet){

        String[] fullAnnot= new String[monSize];
        int[] condensedAnnot= new int[monSize];

        for (MSFragment msFragment: fragmentSet){
            List<MonomerAnnotation> monomerAnnotationList=msFragment.getMonomerAnnotations();
            for(int i=0;i<msFragment.getMonomerAnnotations().size();i++){
                MonomerAnnotation monomerAnnotation=monomerAnnotationList.get(i);
                int monIdx=monomerAnnotation.getIdx();
                fullAnnot[monIdx-1]=monomerAnnotation.getSymbol()+"["+monIdx+"]";
                condensedAnnot[monIdx-1]=monIdx;
            }
        }

        String full = Stream.of(fullAnnot)
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining(";"));
        String condensed=sequenceNums(condensedAnnot);

        Annotation annotation= new Annotation(condensed,full,condensedAnnot);
        return annotation;
    }


    private static void appendRange(StringBuilder sb, int begin, int end) {
        sb.append(",").append(begin);
        int diff=Math.abs(end - begin);
        if (diff >1){
            sb.append("-").append(end);
        }else if(diff!=0){
            sb.append(",").append(end);
        }

    }
    public static String sequenceNums(int[] nums) {
        StringBuilder sb = new StringBuilder();
        if (nums.length == 0) return sb.toString();
        int begin=0;
        int end=0;
        int last=5000;
        boolean firstIter= true;
        for (int cur : nums) {
            if(cur!=0){
                if(firstIter){
                    begin = end = cur;
                    firstIter=false;
                }
                if (cur - end <= 1) {
                    end = cur;
                } else {
                    appendRange(sb, begin, end);
                    begin = end = cur;
                }
                last=cur;
            }
        }
        if(last==end)
            appendRange(sb, begin, end);
        return "["+sb.substring(1)+"]";
    }


    public Annotation annotateFragWithComp(Set<MSFragment> fragmentSet){

        Set<String>fragments= new HashSet<>();
        for (MSFragment msFragment: fragmentSet){
            for(int i=0;i<msFragment.getMonomerAnnotations().size();i++){
                fragments.add(msFragment.getIndex()+"."+i);

            }
        }

        String[] fullAnnot= new String[monSize];
        int[] condensedAnnot= new int[monSize];
        int c= 0;
        for (MSFragment msFragment: fragmentSet){

            List<MonomerAnnotation> monomerAnnotationList=msFragment.getMonomerAnnotations();

            for(int i=0;i<msFragment.getMonomerAnnotations().size();i++){

                if(fragments.contains(msFragment.getIndex()+"."+i)){

                    MonomerAnnotation monomerAnnotation=monomerAnnotationList.get(i);
                    int monIdx=monomerAnnotation.getIdx();

                    List<Composition> formulas= new ArrayList<>();
                    boolean allPresent=true;
                    for (String frag: monomerAnnotation.getFragmentsInMonomer()){
                        if(fragments.contains(frag)){
                            formulas.add(fragCompositions.get(frag));
                            fragments.remove(frag);
                        }else{
                            allPresent=false;
                        }
                    }
                    if(allPresent){
                        fullAnnot[monIdx-1]=monomerAnnotation.getSymbol()+"["+monIdx+"]";
                        condensedAnnot[monIdx-1]=monIdx;
                        c++;

                    }else{
                        Composition composition= new Composition(formulas.toArray(new Composition[formulas.size()]));
                        //fullAnnot[monIdx-1]="("+composition.getFormula()+")"+"["+monIdx+"]";//do that if you want the other kind of annotation
                        condensedAnnot[monIdx-1]=monIdx;
                    }
                }
            }

        }

        String full = Stream.of(fullAnnot)
                .filter(s -> s != null && !s.isEmpty())
                .collect(Collectors.joining(";"));
        String condensed=sequenceNumsComp(condensedAnnot,fullAnnot);
        Annotation annotation= new Annotation(condensed,full,condensedAnnot);
        return annotation;
    }

    public static String sequenceNumsComp(int[] nums, String[] notFullMon) {
        StringBuilder sb = new StringBuilder();
        if (nums.length == 0) return sb.toString();
        int begin=0;
        int end=0;
        int last=5000;
        boolean firstIter= true;
        int c =0;
        for (int cur : nums) {
            if(cur!=0){
                if(firstIter){
                    begin = end = cur;
                    firstIter=false;
                }
                if (cur - end <= 1) {
                    end = cur;

                } else {
                    appendRange(sb, begin, end);
                    begin = end = cur;
                }
                last=cur;
            }else if(notFullMon[c]!=null){
                if(begin!=0){
                    appendRange(sb, begin, end);
                    begin = end =end+2;
                }
                sb.append(",").append(notFullMon[c]);
            }
            c++;
        }
        if(last==end)
            appendRange(sb, begin, end);
        return sb.substring(1);
    }


}


