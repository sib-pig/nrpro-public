package annotation;

import ion.Terminus;

import java.util.Arrays;

public class IonAnnotation {
    private String ionAnnot;
    private String bondPosition;
    private String wholeAnnot;
    public IonAnnotation(String ionAnnot, Terminus terminus) {
        this.ionAnnot=ionAnnot;
        this.bondPosition= Arrays.toString(terminus.getBondPosition())
                .replace("[", "(")
                .replace("]", ")").replace(" ", "");
        this.wholeAnnot=this.bondPosition+ionAnnot;
    }

    public String getIonAnnot() {
        return ionAnnot;
    }
    public String getBondPosition() {
        return bondPosition;
    }
    public String getWholeAnnot() {
        return wholeAnnot;
    }

}
