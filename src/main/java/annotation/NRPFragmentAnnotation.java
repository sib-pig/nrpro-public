package annotation;
import graph.NRPFragmentId;
import org.expasy.mzjava.core.mol.Composition;
import graph.NRPFragment;
import org.expasy.mzjava.core.ms.peaklist.PeakAnnotation;


/**
 * Created by ericart on 17.03.2016.
 */

public class NRPFragmentAnnotation implements PeakAnnotation{

    private double theoreticalMz;
    private double theoreticalMass;
    private final int charge;
    private final NRPFragment fragment;
    private String neutralLosses;
    private Composition composition;
    private int annotScore;
    private boolean isIsotope;

    public NRPFragmentAnnotation(int charge,boolean isIsotope){

        this.charge=charge;
        this.isIsotope=isIsotope;
        this.fragment=null;
    }
    public NRPFragmentAnnotation(double theoreticalMz, double theoreticalMass, int charge, NRPFragment fragment, String neutralLosses, Composition composition,int annotScore) {
        this.theoreticalMz = theoreticalMz;
        this.theoreticalMass = theoreticalMass;
        this.charge = charge;
        this.fragment = fragment;
        this.neutralLosses = neutralLosses;
        this.composition=composition;
        this.annotScore=annotScore;
        this.isIsotope=false;
    }

    public double getTheoreticalMz() {
        return theoreticalMz;
    }

    public double getTheoreticalMass() {
        return theoreticalMass;
    }

    @Override
    public int getCharge() {
        return charge;
    }

    public NRPFragment getFragment() {
        return fragment;
    }


    public NRPFragmentId getFragmentId() {
        return fragment.getNrpFragmentId();
    }

    public String getNeutralLosses() {
        return neutralLosses;
    }

    public Composition getComposition() { return composition; }

    public int getAnnotScore() {
        return annotScore;
    }

    public final NRPFragmentAnnotation copy() {
        return new NRPFragmentAnnotation(this.theoreticalMz,this.theoreticalMass,this.charge,this.fragment,this.neutralLosses,this.composition,this.annotScore);
    }

    public boolean isIsotope() {
        return isIsotope;
    }

    @Override
    public String toString() {
        return "theoreticalMz=" + theoreticalMz +
                ", theoreticalMass=" + theoreticalMass +
                ", charge=" + charge +
                ", neutralLosses='" + neutralLosses + '\'' +
                ", composition=" + composition.toString() +
                ", annotScore=" + annotScore +
                ", isIsotope=" + isIsotope ;
    }
}
