package bond;

import ion.Terminus;
import molecules.bond.BondType;

public class BondRule {
    private BondType bondType ;
    private Terminus[] terminals;

    public BondRule() { }

    public BondType getBondType() {
        return bondType;
    }

    public Terminus[] getTerminals() {
        return terminals;
    }

    public Terminus getTerminus(MSFragmentPosition msFragmentPosition) {
        return terminals[msFragmentPosition.getPosition()];
    }

}
