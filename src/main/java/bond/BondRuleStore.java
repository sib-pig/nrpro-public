package bond;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReaderNRPro;
import ion.Terminus;
import molecules.bond.BondType;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BondRuleStore {
    private static BondRuleStore ourInstance = new BondRuleStore();
    private final Map<BondType,BondRule> rulesMap= new HashMap<>();
    private String rulesResource=System.getProperty("BondRules.store.resource", "bondRules.json");
    private JsonReaderNRPro jsonReaderNRPro = new JsonReaderNRPro();

    public static BondRuleStore getInstance() {
        return ourInstance;
    }

    private BondRuleStore()  {
        try {
            loadRules();
        }catch (IOException e){
            throw new IllegalStateException("Rules file could not be loaded", e);
        }

    }

    //used when we read locally
    private void loadRules() throws IOException {
        InputStream fileRules  = this.getClass().getResourceAsStream(rulesResource);
        JsonNode rootnode = jsonReaderNRPro.readFile(fileRules);
        addRules(rootnode);

    }

    //helper method to add the NRPs to the map (NRPMap)
    private void addRules(JsonNode rootnode) throws IOException {

        List<BondRule> listrules = jsonReaderNRPro.jsonNode2list(rootnode, BondRule.class);
        for (BondRule rule : listrules) {
            if(!rulesMap.containsKey(rule.getBondType())){
                this.rulesMap.put(rule.getBondType(),rule);
            }
        }

    }
    public Terminus getBondRearrangement(BondType bondType, MSFragmentPosition msFragmentPosition, int[] bondsPosition) throws IOException {
        if(rulesMap.containsKey(bondType)){
            return new Terminus(rulesMap.get(bondType).getTerminus(msFragmentPosition), bondsPosition);
        }else{
            throw new IOException("Unknown bond!");
        }

    }

}
