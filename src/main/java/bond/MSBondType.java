package bond;

public enum MSBondType {
    AMINO,
    ESTER,
    THIOETHER,
    GLYCOSIDIC,
    DISULFUR,
    UNKNOWN;
}
