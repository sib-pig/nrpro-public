package bond;

public enum MSFragmentPosition {
    SOURCE(0),
    TARGET(1);

    private final int position;

    MSFragmentPosition(int position) {
        this.position = position;
    }

    public int getPosition() {
        return this.position;
    }

}
