package db;

public enum Database {
    Norine,
    NPAtlas,
    ChEBI,
    PubChem;
}
