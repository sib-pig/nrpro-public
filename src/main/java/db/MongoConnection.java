package db;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoConnection {
    private static MongoConnection ourInstance = new MongoConnection();
    private MongoClient client;
    private MongoDatabase database;
    public static MongoConnection getInstance() {

        return ourInstance;
    }

    private MongoConnection() {
        System.out.println("creating client");
        client= new MongoClient("localhost", 27017);
        database= client.getDatabase("nrpro");

    }
    public MongoCollection getCollection(String name){

        return database.getCollection(name);

    }
    public void close(){
        System.out.println("close...");
        client.close();
    }

}
