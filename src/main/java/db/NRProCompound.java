package db;

import interfaces.MSCompound;
import interfaces.Monomeric;
import mapping.AtomicGraph;

import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.util.Map;
import java.util.Set;

public class NRProCompound implements MSCompound, Monomeric {
    private String id;
    private String name;
    private Set<String> synonyms;
    private Map<Database, Set<String>> externalIds;
    private String smiles;
    private int size;
    private double coverage;
    private double correctness;
    private double monoisotopicMass;
    private String formula;
    //@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private SimpleDirectedGraph<MSFragment, MSBond> monomericGraph;
    private AtomicGraph atomicGraph;
    private String category;//norine
    private Set <String> activities;//norine
    private String structureType;//norine
    private Set <String> organism;//npatlas, norine
    private Set <String> origin;//npatlas
    private Set <String> associatedSpectra;//npatlas




    public NRProCompound() {
    }

    public NRProCompound(String id, String name, Set<String> synonyms, Map<Database, Set<String>> externalIds, String smiles,
                         int size, double coverage, double correctness, double monoisotopicMass, String formula,
                         SimpleDirectedGraph<MSFragment, MSBond> monomericGraph, AtomicGraph atomicGraph, String category,
                         Set <String>  activities, String structureType, Set <String> organism, Set <String> origin,Set <String> associatedSpectra) {
        this.id = id;
        this.name = name;
        this.synonyms = synonyms;
        this.externalIds = externalIds;
        this.smiles = smiles;
        this.size = size;
        this.coverage = coverage;
        this.correctness = correctness;
        this.monoisotopicMass = monoisotopicMass;
        this.formula = formula;
        this.monomericGraph = monomericGraph;
        this.atomicGraph = atomicGraph;
        this.category = category;
        this.activities = activities;
        this.structureType = structureType;
        this.organism = organism;
        this.origin = origin;
        this.associatedSpectra= associatedSpectra;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Set<String> getSynonyms() {
        return synonyms;
    }

    public Map<Database, Set<String>> getExternalIds() {
        return externalIds;
    }

    public String getSmiles() {
        return smiles;
    }

    public int getSize() { return size; }

    public double getCoverage() {
        return coverage;
    }

    public double getCorrectness(){return correctness;}

    public double getMonoisotopicMass() {
        return monoisotopicMass;
    }

    public String getFormula() {
        return formula;
    }

    public SimpleDirectedGraph<MSFragment, MSBond> getMonomericGraph() {
        return monomericGraph;
    }

    public AtomicGraph getAtomicGraph() {
        return atomicGraph;
    }

    public String getCategory() {
        return category;
    }

    public Set <String> getActivities() {
        return activities;
    }

    public String getStructureType() {
        return structureType;
    }

    public Set <String> getOrganism() {
        return organism;
    }

    public Set <String> getOrigin() {
        return origin;
    }

    public Set<String> getAssociatedSpectra() {
        return associatedSpectra;
    }
}
