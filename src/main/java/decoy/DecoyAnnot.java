package decoy;

public class DecoyAnnot {
    private double mz;
    private String neutralLoss;
    private int charge;

    public DecoyAnnot() {
    }

    public DecoyAnnot(double mz, String neutralLoss, int charge) {
        this.mz = mz;

        this.neutralLoss = neutralLoss;
        this.charge = charge;
    }

    public double getMz() {
        return mz;
    }

    public String getNeutralLoss() {
        return neutralLoss;
    }

    public int getCharge() {
        return charge;
    }
}
