package decoy;

import interfaces.Monomeric;
import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import org.jgrapht.graph.SimpleDirectedGraph;

public class DecoyCompound implements Monomeric {
    private String originalCompId;
    private double monoisotopicMass;
    private String formula;
    private SimpleDirectedGraph<MSFragment, MSBond> monomericGraph;
    private int size;

    public DecoyCompound() {
    }

    public DecoyCompound(String originalCompId, double monoisotopicMass, String formula, SimpleDirectedGraph<MSFragment, MSBond> monomericGraph, int size) {
        this.originalCompId = originalCompId;
        this.monoisotopicMass = monoisotopicMass;
        this.formula = formula;
        this.monomericGraph = monomericGraph;
        this.size = size;
    }

    public String getOriginalCompId() {
        return originalCompId;
    }

    @Override
    public double getMonoisotopicMass() {
        return monoisotopicMass;
    }

    @Override
    public String getFormula() {
        return formula;
    }

    @Override
    public SimpleDirectedGraph<MSFragment, MSBond> getMonomericGraph() {
        return monomericGraph;
    }

    @Override
    public int getSize() {
        return size;
    }
}
