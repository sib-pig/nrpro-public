package decoy;

import graph.NRPFragmentId;
import ion.Tolerance;
import misc.Adduct;
import misc.NeutralLoss;
import misc.Ppm2DaConverter;

import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DecoySpectrumMatcherHelper {

    private double [] experimentalMzs;
    private double originalTolerance;
    private boolean isInPpm=false;

    public DecoySpectrumMatcherHelper(Tolerance originaltolerance)
    {
        this.originalTolerance=originaltolerance.getTolerance();
        if(originaltolerance.getUnitTolerance()== Tolerance.UnitTolerance.PPM){
            isInPpm=true;
        }
    }

    public double match(DecoyTree annotatedTree, ConsensusSpectrum experimentalSpectrum, int charge, List<NeutralLoss> neutralLosses, List<Adduct>adducts){
        Set<String> nlSet= new HashSet<>();
        for(NeutralLoss neutralLoss : neutralLosses){
            nlSet.add("-"+neutralLoss.toString());
        }
        for(Adduct adduct : adducts){
            nlSet.add("+"+adduct.toString());
        }
        this.experimentalMzs= experimentalSpectrum.getMzs(new double[experimentalSpectrum.size()]);
        Set<NRPFragmentId> matchedFrags= new HashSet<>();
        boolean[]matchedPeaks= new boolean[experimentalSpectrum.size()];
        BreadthFirstIterator<DecoyTreeNode, DecoyTreeEdge> treeIterator = new BreadthFirstIterator(annotatedTree.getDecoyFragTree(), annotatedTree.getRoot());
        double dotProduct=0;
        boolean isParentScored=true;
        double tolerance=this.originalTolerance;
        while (treeIterator.hasNext()){
            DecoyTreeNode decoyNode=treeIterator.next();
            DecoyTreeNode decoyParent=treeIterator.getParent(decoyNode);
            //System.out.println(decoyNode.getDepth());
            if(decoyParent !=null && matchedFrags.contains(decoyParent.getNrpFragmentId())){
                isParentScored=true;
            }

            if(isParentScored || decoyNode.getDepth()<2){
                List<DecoyAnnot>[] annotatedIdxs= decoyNode.getChargeAnnotations();
                List<DecoyAnnot> decoyAnnots= new ArrayList<>();
                for(int i =0;i<charge;i++){
                    for (DecoyAnnot decoyAnnot:annotatedIdxs[i] ){
                        if(decoyAnnot.getNeutralLoss()==null || nlSet.contains(decoyAnnot.getNeutralLoss())){
                            decoyAnnots.add(decoyAnnot);
                        }
                    }
                }
                for (DecoyAnnot decoyAnnot: decoyAnnots){
                    double theoreticalMz=decoyAnnot.getMz();
                    if(isInPpm){
                        tolerance= Ppm2DaConverter.convert(this.originalTolerance,theoreticalMz);
                    }
                    double minMz=theoreticalMz-tolerance;
                    double maxMz=theoreticalMz+tolerance;
                    int matchedExpIdx= binarySearch(minMz,maxMz);

                    if(matchedExpIdx>=0){//matches
                        if(!matchedPeaks[matchedExpIdx]){
                            dotProduct=dotProduct+experimentalSpectrum.getIntensity(matchedExpIdx);
                            matchedPeaks[matchedExpIdx]=true;
                        }
                       matchedFrags.add(decoyNode.getNrpFragmentId());
                    }
                }
            }
            isParentScored=false;
        }
        return dotProduct;
    }

    private int binarySearch(double min, double max) {
        int low = 0;
        int high = this.experimentalMzs.length - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            double midVal = this.experimentalMzs[mid];

            if (midVal < min && midVal < max)
                low = mid + 1;  // Neither val is NaN, thisVal is smaller
            else if (midVal > min && midVal > max)
                high = mid - 1; // Neither val is NaN, thisVal is larger
            else {
                return mid;             // Key found
            }
        }
        return -(low + 1);  // key not found.
    }

}
