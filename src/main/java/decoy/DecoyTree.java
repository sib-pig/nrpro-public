package decoy;

import ion.DissociationTechnique;
import org.jgrapht.graph.SimpleDirectedGraph;

public class DecoyTree {
    private DecoyTreeNode root;
    private DissociationTechnique dissociationTechnique;
    private double precursorMass;
    private SimpleDirectedGraph<DecoyTreeNode, DecoyTreeEdge> decoyFragTree;
    private int treeSize;

    public DecoyTree(DecoyTreeNode root, DissociationTechnique dissociationTechnique, double precursorMass, SimpleDirectedGraph<DecoyTreeNode, DecoyTreeEdge> decoyFragTree, int sizeTree) {
        this.root = root;
        this.dissociationTechnique = dissociationTechnique;
        this.precursorMass = precursorMass;
        this.decoyFragTree = decoyFragTree;
        this.treeSize= sizeTree;
    }

    public DecoyTreeNode getRoot() {
        return root;
    }

    public DissociationTechnique getDissociationTechnique() {
        return dissociationTechnique;
    }

    public double getPrecursorMass() {
        return precursorMass;
    }

    public SimpleDirectedGraph<DecoyTreeNode, DecoyTreeEdge> getDecoyFragTree() {
        return decoyFragTree;
    }

    public int getTreeSize() {
        return treeSize;
    }
}
