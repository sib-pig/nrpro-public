package decoy;

import org.jgrapht.graph.DefaultEdge;

public class DecoyTreeEdge extends DefaultEdge {

    private DecoyTreeNode monomericGraph1;
    private DecoyTreeNode monomericGraph2;

    public DecoyTreeEdge(DecoyTreeNode monomericGraph1, DecoyTreeNode monomericGraph2) {
        this.monomericGraph1 = monomericGraph1;
        this.monomericGraph2 = monomericGraph2;
    }
}
