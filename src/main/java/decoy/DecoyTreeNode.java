package decoy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import graph.NRPFragmentId;

import java.util.List;

public class DecoyTreeNode {

    private NRPFragmentId nrpFragmentId;
    @JsonIgnore
    private NRPFragmentId parentId;
    private int depth;
    @JsonIgnore
    private double mass;
    private List<DecoyAnnot>[] chargeAnnotations;

    public DecoyTreeNode() {
    }

    public DecoyTreeNode(NRPFragmentId nrpFragmentId, NRPFragmentId parentId, double mass, int depth, List<DecoyAnnot>[] chargeAnnotations) {
        this.nrpFragmentId = nrpFragmentId;
        this.mass=mass;
        this.chargeAnnotations = chargeAnnotations;
        this.parentId=parentId;
        this.depth=depth;
    }

    public NRPFragmentId getNrpFragmentId() {
        return nrpFragmentId;
    }

    public int getDepth() {
        return depth;
    }

    public double getMass() {
        return mass;
    }

    public NRPFragmentId getParentId() {
        return parentId;
    }

    public List<DecoyAnnot>[] getChargeAnnotations() {
        return chargeAnnotations;
    }

    public void setParentId(NRPFragmentId parentId) {
        this.parentId = parentId;
    }
}
