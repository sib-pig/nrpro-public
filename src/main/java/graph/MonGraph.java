package graph;

import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import org.expasy.mzjava.core.mol.Composition;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.util.*;

/**
 * Created by ericart on 24.11.2015.
 */

public abstract class MonGraph {

    private MonGraphId graphId;
    private SimpleDirectedGraph<MSFragment, MSBond> graph;
    private Map<Integer,MSFragment> indexesMap;
    private Composition composition;
    private double molecularMass;
    private int size;

    public MonGraph(SimpleDirectedGraph<MSFragment, MSBond> graph, Composition composition) {
        this.graph = graph;
        this.composition = composition;
        this.molecularMass=composition.getMolecularMass();
        this.size=graph.vertexSet().size();
        createIndexesMap();

    }

    public void createIndexesMap(){
        indexesMap =new HashMap<>();
        for (MSFragment monomer : graph.vertexSet()){
            indexesMap.put(monomer.getIndex(),monomer);
        }
    }

    public MonGraphId getGraphId() {
        return graphId;
    }

    public void setGraphId(MonGraphId graphId) {
        this.graphId = graphId;
    }

    public SimpleDirectedGraph<MSFragment, MSBond> getSimpleGraph(){

        return this.graph;

    }
    public SimpleDirectedGraph<MSFragment, MSBond> cloneSimpleGraph(){

        return (SimpleDirectedGraph)this.graph.clone();

    }
    public Set<Integer> getMonomerIdxs(){
        return indexesMap.keySet();
    }

    public MSFragment getMonomer(int monomerIdx){

        return indexesMap.get(monomerIdx);

    }

    public Composition getComposition() {

        return composition;
    }


    public double getMolecularMass() {

        return molecularMass;

    }
    public Set<MSFragment> getAllMonomerNode(){

        return this.graph.vertexSet();

    }

    public Set<MSBond> getAllBond(){

        return this.graph.edgeSet();

    }


    public Set<MSBond> getBondsSubset(Set<MSFragment> monomerSubset){
        Set<MSBond> bondsSubset = new HashSet<>();
        for (MSFragment node : this.graph.vertexSet()){
            if (monomerSubset.contains(node)){

                for (MSBond edge : this.graph.edgesOf(node)){
                    MSFragment monomer1 = edge.getSource();//why returns an object and needs to be cast?
                    MSFragment monomer2 = edge.getTarget();
                    if (monomerSubset.contains(monomer1) && monomerSubset.contains(monomer2)){
                        bondsSubset.add(edge);

                    }
                }
            }
        }
        return bondsSubset;
    }

    public int size() {
        return size;
    }


    public boolean containsMonomer(MSFragment monomer){
        return this.graph.containsVertex(monomer);
    }

    public boolean isEndMolecule(){
        return this.graph.vertexSet().size() == 1;
    }

    @Override
    public String toString() {
        return this.graph.vertexSet().toString()+" "+this.graph.edgeSet().toString();
    }

    /**
     * Created by ericart on 10.03.2016.
     */
    public static class Loss extends DefaultEdge {
        private MonGraph monomericGraph1;
        private MonGraph monomericGraph2;
        MSBond lostEdge;

        public Loss(MonGraph m1, MonGraph m2, MSBond lostEdge) {
            this.monomericGraph1 = m1;
            this.monomericGraph2 = m2;
            this.lostEdge = lostEdge;
        }

        public MonGraph getM1() {
            return monomericGraph1;
        }

        public MonGraph getM2() {
            return monomericGraph2;
        }

        public MSBond getLostEdge() {
            return lostEdge;
        }


        public String toString() {

            return this.lostEdge.toString();
        }
    }
}
