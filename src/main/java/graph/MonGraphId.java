package graph;

import java.util.Objects;

public class MonGraphId {

    private int i;

    public MonGraphId(int i) {
        this.i = i;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MonGraphId graphId = (MonGraphId) o;
        return i == graphId.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(i);
    }
}
