package graph;

import annotation.IonAnnotation;
import com.fasterxml.jackson.annotation.JsonIgnore;
import annotation.Annotation;
import bond.BondRuleStore;
import bond.MSFragmentPosition;
import ion.Terminus;
import molecules.bond.BondType;
import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import annotation.NRPFragmentAnnotation;
import org.expasy.mzjava.core.mol.Composition;

import org.jgrapht.graph.SimpleDirectedGraph;

import java.io.IOException;
import java.util.*;


/**
 * Created by ericart on 19.11.2015.
 */
public class NRPFragment extends MonGraph {

    private NRPFragmentId nrpFragmentId;
    private int depth;
    private Annotation annotation;
    private List<Terminus> breakage = Collections.emptyList();
    @JsonIgnore
    private Map<MSFragment,List<Terminus>> terminals = Collections.emptyMap();
    private Map<Double,NRPFragmentAnnotation> resultingFragments= new TreeMap<>();//ascending order


    private NRPFragment(SimpleDirectedGraph<MSFragment, MSBond> fragmentSubgraph, int depth,Annotation annotation, Composition composition, Map<MSFragment,List<Terminus>> teminals, List<Terminus> breakage) {
        super(fragmentSubgraph, composition);
        this.depth= depth;
        this.annotation=annotation;
        this.terminals = teminals;
        this.breakage=breakage;
    }

    public NRPFragment(SimpleDirectedGraph<MSFragment, MSBond> fragmentSubgraph, Composition composition, int depth,Annotation annotation) {
        super(fragmentSubgraph, composition);
        this.depth= depth;
        this.annotation=annotation;

    }

    public int getDepth() {
        return depth;
    }

    public Annotation getAnnotation() {
        return annotation;
    }

    public Map<MSFragment, List<Terminus>> getTerminals() {
        return terminals;
    }

    public List<Terminus> getBreakage() {
        return breakage;
    }

    public Set<Double> getMzs() {
        return resultingFragments.keySet();
    }
    public NRPFragmentAnnotation getNRPFragmentAnnotation(double mz) {
        return resultingFragments.get(mz);
    }

    public void addResultingFragment(double mz, NRPFragmentAnnotation nrpFragmentAnnotation){
        this.resultingFragments.put(mz,nrpFragmentAnnotation);
    }

    public NRPFragmentId getNrpFragmentId() {
        return nrpFragmentId;
    }

    public void setNrpFragmentId(NRPFragmentId nrpFragmentId) {
        this.nrpFragmentId = nrpFragmentId;
    }

    public static class Builder{

        SimpleDirectedGraph<MSFragment, MSBond> fragmentSubgraph;
        Composition originalComposition;
        Map<MSFragment,List<Terminus>> terminals =new HashMap<>();
        Annotation annotation=null;
        List<IonAnnotation> ionAnnot= new ArrayList();
        List<String> bondPosition= new ArrayList<>();
        List<Terminus> breakage= new ArrayList<>();
        int depth;


        public Builder (SimpleDirectedGraph<MSFragment, MSBond> fragmentSubgraph, Composition originalComposition,int depth){
            this.fragmentSubgraph=fragmentSubgraph;
            this.originalComposition = originalComposition;
            this.depth=depth;
        }

        public void setAnnotation (Annotation annotation){
            this.annotation=annotation;
        }


        public void addNewRearrangement(MSFragment monomer, BondType bondType, MSFragmentPosition msFragmentPosition, int[] monIdxs) throws IOException {

            Terminus terminus= BondRuleStore.getInstance().getBondRearrangement(bondType, msFragmentPosition,monIdxs);
            addOldRearrangement(monomer,terminus);
        }

        public void addOldRearrangement(MSFragment monomer, Terminus terminus){

            this.breakage.add(terminus);
            String[] splitIon=terminus.getIonAnnotation().toString().split("_");
            String iAnnot=splitIon[0].concat("(").concat(splitIon[1]).concat(")");
            IonAnnotation ionAnnotation = new IonAnnotation(iAnnot,terminus);
            this.ionAnnot.add(ionAnnotation);

            if (this.terminals.containsKey(monomer)){
                this.terminals.get(monomer).add(terminus);
            }else{
                List<Terminus> terminusList = new ArrayList<>();
                terminusList.add(terminus);
                this.terminals.put(monomer, terminusList);
            }

        }

        public NRPFragment build(){
            if(annotation!=null){
                setIons();
            }
            return new NRPFragment(this.fragmentSubgraph, this.depth,this.annotation, this.originalComposition,this.terminals,this.breakage);
        }

        private void setIons(){
            StringBuilder stringBuilder= new StringBuilder();
            Comparator<IonAnnotation> compareById = Comparator.comparing(IonAnnotation::getIonAnnot);
            Collections.sort(this.ionAnnot, compareById.reversed());
            for(IonAnnotation iAnnot : this.ionAnnot){
                stringBuilder.append(iAnnot.getWholeAnnot());
            }
            this.annotation.setIonAnnot(stringBuilder.toString()+this.annotation.getCondensed());
        }

    }



}
