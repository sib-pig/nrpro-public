package graph;

import java.util.Objects;

public class NRPFragmentId {

    private int id;

    public NRPFragmentId() {
    }

    public NRPFragmentId(int i) {
        this.id = i;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NRPFragmentId that = (NRPFragmentId) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
