package graph;

import interfaces.Monomeric;
import org.jgrapht.graph.SimpleDirectedGraph;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.util.Set;

/**
 * Created by ericart on 18.04.2016
 */

public class NRPFragmentTree {

    private Monomeric nrp;
    private NRPFragment root;
    private SimpleDirectedGraph<NRPFragment, MonGraph.Loss> tree;

    public NRPFragmentTree(Monomeric nrp, NRPFragment root,  SimpleDirectedGraph<NRPFragment, MonGraph.Loss> tree) {
        this.nrp = nrp;
        this.root = root;
        this.tree = tree;

    }

    public Monomeric getNRP() {
        return nrp;
    }

    public NRPFragment getRoot() {
        return root;
    }

    public boolean isRoot(NRPFragment nrpFragment) {
        return this.tree.inDegreeOf(nrpFragment)==0 ? true : false;
    }

    public BreadthFirstIterator<NRPFragment, MonGraph.Loss>  getBreathFirstIterator(){
        return new BreadthFirstIterator(this.tree,this.root);
    }

    public Set<NRPFragment> getFragments(){
        return this.tree.vertexSet();
    }


}
