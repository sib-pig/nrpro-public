package interfaces;

public interface MSCompound {
    String getId();
    String getName();
    String getSmiles();
    double getMonoisotopicMass();
}
