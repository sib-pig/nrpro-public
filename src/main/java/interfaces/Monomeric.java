package interfaces;

import molecules.bond.MSBond;
import molecules.monomer.MSFragment;
import org.jgrapht.graph.SimpleDirectedGraph;

/**
 * Created by ericart on 06.04.2016.
 */
public interface Monomeric {
    int getSize();
    double getMonoisotopicMass();
    String getFormula();
    SimpleDirectedGraph<MSFragment, MSBond> getMonomericGraph();
}
