package io;


import java.io.IOException;
import java.io.InputStream;


public class ExampleFilesStore {
    private static ExampleFilesStore ourInstance = new ExampleFilesStore();
    private InputStream[] exampleFiles= new InputStream[3];
    private String[] names= new String[3];
    public static ExampleFilesStore getInstance() {
        return ourInstance;
    }

    private ExampleFilesStore()  {
        try {
            loadFiles();
        }catch (IOException e){
            throw new IllegalStateException("Rules file could not be loaded", e);
        }

    }
    //used when we read locally
    private void loadFiles() throws IOException {
        names[0]="Cyclosporin_A.mgf";
        exampleFiles[0] = this.getClass().getResourceAsStream(names[0]);
        names[1]="Gramicidin_C.mgf";
        exampleFiles[1] = this.getClass().getResourceAsStream(names[1]);
        names[2]="Pseudacyclin_A.mgf";
        exampleFiles[2] = this.getClass().getResourceAsStream(names[2]);
    }
    public int getSize(){
        return exampleFiles.length;
    }
    public InputStream getExampleFile(int i ){
        return exampleFiles[i];
    }
    public String getNameExampleFile(int i){
        return names[i];
    }

}
