package io;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.IOUtils;


import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by ericart on 21.10.2015.
 */

/**
 * This class can read a json file either from a REST or a file. Once obtained the JsonNode, if it contains
 * an array of objects, it is also possible to store this objects in its correspondent class using jsonNode2list, which
 * returns the list of target objects.
 */

public class JsonReaderNRPro<C> {

    NRProCompoundMapper objectMapper = new NRProCompoundMapper();


    public JsonReaderNRPro() {
        //we use that option so it not complains if we do not store all the information of an object
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

    }

    public JsonNode readFile (Path path)throws IOException{
        byte[] jsonData = Files.readAllBytes(path);
        return objectMapper.readTree(jsonData);
    }

    public JsonNode readFile (InputStream file) throws IOException {

        byte[] jsonData=IOUtils.toByteArray(file);
        return objectMapper.readTree(jsonData);

    }


    public List<C> jsonNode2list(JsonNode node, Class<C> c) throws IOException{
        List<C> list = new ArrayList<>();
        for (JsonNode n : node) {
            C o = objectMapper.readValue(n.toString(), c);
            list.add(o);
        }
        return list;

    }

}
