package io;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.compositon.CompositionModule;
import io.decoyTree.DecoyTreeModule;
import io.misc.FragmentGraphModule;
import io.modules.AtomicGraphModul;
import io.spectrum.SpectrumModule;


public class NRProCompoundMapper extends ObjectMapper {
    public NRProCompoundMapper() {
        this.registerModule(new AtomicGraphModul());
        this.registerModule(new FragmentGraphModule());
        this.registerModule(new SpectrumModule());
        this.registerModule(new CompositionModule());
        this.registerModule(new DecoyTreeModule());
    }
}
