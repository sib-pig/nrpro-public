package io;

import org.apache.commons.io.FilenameUtils;
import org.expasy.mzjava.core.io.ms.spectrum.AbstractMsReader;
import org.expasy.mzjava.core.io.ms.spectrum.MgfReader;
import org.expasy.mzjava.core.io.ms.spectrum.MzxmlReader;
import org.expasy.mzjava.core.ms.peaklist.PeakList;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class SpectrumReader {


    public List<MsnSpectrum> readSpectrum(InputStream inputStream,String  nameFile) throws IOException, URISyntaxException {
        Reader reader = new InputStreamReader(inputStream);
        String extension=FilenameUtils.getExtension(nameFile);
        SpectrumFormat format= SpectrumFormat.valueOf(extension.toLowerCase());
        if(format.equals(SpectrumFormat.mgf)) {
            return readMgf(reader,nameFile);
        }  else if (format.equals(SpectrumFormat.mzxml)){
            return readMzXML(reader,nameFile);
        }
        return null;
    }


    private List<MsnSpectrum>  readMgf(Reader reader,String  nameFile) throws IOException, URISyntaxException {
        MgfReader mgfReader= new MgfReader(reader, new URI(nameFile), PeakList.Precision.FLOAT);
        return read(mgfReader);
    }

    private  List<MsnSpectrum>  readMzXML(Reader reader, String nameFile) throws URISyntaxException {


        try {
            Constructor<MzxmlReader> ctor=MzxmlReader.class.getDeclaredConstructor(Reader.class,URI.class,PeakList.Precision.class);
            ctor.setAccessible(true);
            return read(ctor.newInstance(reader,new URI(nameFile),PeakList.Precision.FLOAT));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<MsnSpectrum>read( AbstractMsReader abstractMsReader) throws IOException {

        abstractMsReader.acceptUnsortedSpectra();
        List<MsnSpectrum> experimentalSpectra = new ArrayList<>();
        while (abstractMsReader.hasNext()) {
            MsnSpectrum spectra = (MsnSpectrum) abstractMsReader.next();
            experimentalSpectra.add(spectra);
        }

        return experimentalSpectra;

    }

    public enum SpectrumFormat {
        mgf,
        mzxml;
    }
}






