package io.compositon;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.expasy.mzjava.core.mol.Composition;

import java.io.IOException;

public class CompositionDeserializer extends StdDeserializer<Composition> {
    public CompositionDeserializer() {
        this(null);
    }

    private CompositionDeserializer(Class<?> vc) {
        super(vc);
    }
    @Override
    public Composition deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String composition= node.asText();
        return Composition.parseComposition(composition);
    }

}
