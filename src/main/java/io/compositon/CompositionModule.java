package io.compositon;

import com.fasterxml.jackson.core.util.VersionUtil;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.expasy.mzjava.core.mol.Composition;

public class CompositionModule extends SimpleModule {
    private static final String NAME = "CompositionModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public CompositionModule() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(Composition.class, new CompositionDeserializer());

    }

}
