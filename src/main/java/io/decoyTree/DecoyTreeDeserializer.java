package io.decoyTree;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import decoy.DecoyTreeEdge;
import decoy.DecoyTreeNode;
import decoy.DecoyTree;
import ion.DissociationTechnique;
import org.jgrapht.graph.SimpleDirectedGraph;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DecoyTreeDeserializer extends StdDeserializer<DecoyTree> {

    public DecoyTreeDeserializer() {

        this(null);
    }

    private DecoyTreeDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public DecoyTree deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {

        JsonNode node = jp.getCodec().readTree(jp);
        //String j=node.path("dissociationTechnique").asText();
        DissociationTechnique dissociationTechnique= DissociationTechnique.valueOf(node.path("dissociationTechnique").asText());
        double precursorMass=node.path("precursorMass").asDouble();
        int treeSize=node.path("treeSize").asInt();
        JsonNode nodes=node.path("nodes");
        List<DecoyTreeNode> treeNodes = new ArrayList<>();

        ObjectMapper objectMapper= new ObjectMapper();
        for (JsonNode n: nodes){
            String h=n.toString();
            DecoyTreeNode decoyTreeNode=objectMapper.readValue(n.toString(),DecoyTreeNode.class);
            treeNodes.add(decoyTreeNode);
        }
        SimpleDirectedGraph<DecoyTreeNode, DecoyTreeEdge> decoyFragTree = new SimpleDirectedGraph(DecoyTreeEdge.class);
        decoyFragTree.addVertex(treeNodes.get(0));
        JsonNode treeStructure=node.path("treeStructure");
        int idxNodes=0;
        for(JsonNode jn: treeStructure){
            DecoyTreeNode parent=treeNodes.get(idxNodes);
            for(JsonNode n: jn){
                DecoyTreeNode treeNode=treeNodes.get( n.intValue());
                decoyFragTree.addVertex(treeNode);
                DecoyTreeEdge edge= new DecoyTreeEdge(parent,treeNode);
                decoyFragTree.addEdge(parent,treeNode,edge);
            }
            idxNodes++;
        }

        return new DecoyTree(treeNodes.get(0),  dissociationTechnique, precursorMass, decoyFragTree, treeSize);
    }
}
