package io.decoyTree;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;

import decoy.DecoyTree;


public class DecoyTreeModule  extends SimpleModule {

    private static final String NAME = "DecoyTreeModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {
    };

    public DecoyTreeModule() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(DecoyTree.class, new DecoyTreeDeserializer());
        addSerializer(DecoyTree.class, new DecoyTreeSerializer());
    }
}
