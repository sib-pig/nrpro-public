package io.decoyTree;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import decoy.DecoyTreeEdge;
import decoy.DecoyTreeNode;
import graph.NRPFragmentId;
import decoy.DecoyTree;
import org.jgrapht.traverse.BreadthFirstIterator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DecoyTreeSerializer extends JsonSerializer<DecoyTree> {

    @Override
    public void serialize(DecoyTree decoyTree, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        BreadthFirstIterator<DecoyTreeNode, DecoyTreeEdge> treeIterator = new BreadthFirstIterator(decoyTree.getDecoyFragTree(), decoyTree.getRoot());
        Map<NRPFragmentId,Integer> fragsPositions= new HashMap<>();
        List<Integer>[] integers=new List[decoyTree.getTreeSize()];

        for(int n=0; n<decoyTree.getTreeSize();n++){
            integers[n]=new ArrayList<>();
        }

        //print array decoy nodes here
        DecoyTreeNode root= treeIterator.next();
        fragsPositions.put( root.getNrpFragmentId(),0);

        jsonGenerator.writeObjectField("dissociationTechnique",decoyTree.getDissociationTechnique());
        jsonGenerator.writeNumberField("precursorMass",decoyTree.getPrecursorMass());
        jsonGenerator.writeNumberField("treeSize",decoyTree.getTreeSize());
        jsonGenerator.writeArrayFieldStart("nodes");
        jsonGenerator.writeObject(root);
        DecoyTreeNode parent=root;
        int i=1;
        while(treeIterator.hasNext()){
            //print array decoy nodes here
            DecoyTreeNode decoyTreeNode=treeIterator.next();
            jsonGenerator.writeObject(decoyTreeNode);
            parent=treeIterator.getParent(decoyTreeNode);
            integers[fragsPositions.get(parent.getNrpFragmentId())].add(i);
            fragsPositions.put(decoyTreeNode.getNrpFragmentId(),i);
            i++;
        }

        int lastParentIdx=fragsPositions.get(parent.getNrpFragmentId());
        jsonGenerator.writeEndArray();

        jsonGenerator.writeArrayFieldStart("treeStructure");

        for(int n=0; n<=lastParentIdx;n++){
            jsonGenerator.writeStartArray();
            for(Integer position: integers[n]){
                jsonGenerator.writeNumber(position);
            }
            jsonGenerator.writeEndArray();

        }

        jsonGenerator.writeEndArray();


        jsonGenerator.writeEndObject();

    }
}
