package io.misc;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializerProvider;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import io.NRProCompoundMapper;
import misc.Candidate;

import java.io.IOException;



public class CandidateSerializer  extends JsonSerializer<Candidate> {

    @Override
    public void serialize(Candidate candidate, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("dotproduct",candidate.getDotproduct());
        jsonGenerator.writeNumberField("annotPeaks",candidate.getAnnotPeaks());
        jsonGenerator.writeNumberField("scoredPeaks",candidate.getScoredPeaks());
        jsonGenerator.writeNumberField("isotopePeaks",candidate.getIsotopePeaks());
        jsonGenerator.writeNumberField("pvalue",candidate.getPvalue());
        jsonGenerator.writeNumberField("score",candidate.getScore());
        NRProCompoundMapper objectMapper= new NRProCompoundMapper();

        SimpleBeanPropertyFilter theFilter = SimpleBeanPropertyFilter.serializeAllExcept("monomericGraph");
        FilterProvider filters = new SimpleFilterProvider().addFilter("myFilter", theFilter);

        ObjectWriter writer=objectMapper.writer(filters);

        jsonGenerator.writeFieldName("nrproCompound");
        writer.writeValue(jsonGenerator,candidate.getNrp());

        jsonGenerator.writeFieldName("consensusSpectrum");
        writer.writeValue(jsonGenerator,candidate.getConsensusSpectrum());
        jsonGenerator.writeEndObject();
    }
}
