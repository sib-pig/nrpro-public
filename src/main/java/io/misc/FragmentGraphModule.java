package io.misc;

import io.deserializers.FragmentGraphDeserializer;
import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;
import io.serializers.SimpleDirectedGraphSerializer;
import org.jgrapht.graph.SimpleDirectedGraph;


public class FragmentGraphModule extends SimpleModule {

    private static final String NAME = "FragmentGraphModule";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {};

    public FragmentGraphModule() {
        super(NAME, VERSION_UTIL.version());
        addDeserializer(SimpleDirectedGraph.class, new FragmentGraphDeserializer());
        addSerializer(SimpleDirectedGraph.class, new SimpleDirectedGraphSerializer());
    }

}
