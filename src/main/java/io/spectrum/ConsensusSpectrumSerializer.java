package io.spectrum;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import molecules.monomer.MSFragment;
import annotation.NRPFragmentAnnotation;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.Peak;

import java.io.IOException;
import java.util.List;

public class ConsensusSpectrumSerializer extends JsonSerializer<ConsensusSpectrum> {
    @Override
    public void serialize(ConsensusSpectrum spectrum, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeObjectFieldStart("precursor");
        Peak peak=spectrum.getPrecursor();
        jsonGenerator.writeStringField("polarity",peak.getPolarity().toString());

        int[] chargeList = peak.getChargeList();
        jsonGenerator.writeArrayFieldStart("charge");
        for(int i: chargeList) {
            jsonGenerator.writeNumber(i);
        }
        jsonGenerator.writeEndArray();

        jsonGenerator.writeNumberField("mz",peak.getMz());
        jsonGenerator.writeNumberField("intensity",peak.getIntensity());
        jsonGenerator.writeEndObject();

        jsonGenerator.writeNumberField("msLevel",spectrum.getMsLevel());
        jsonGenerator.writeStringField("name",spectrum.getName());
        jsonGenerator.writeNumberField("simScoreMean",spectrum.getSimScoreMean());
        jsonGenerator.writeNumberField("simScoreStdev",spectrum.getSimScoreStdev());
        jsonGenerator.writeNumberField("precursorMzMean",spectrum.getPrecursorMzMean());
        jsonGenerator.writeNumberField("precursorMzStdev",spectrum.getPrecursorMzStdev());

        jsonGenerator.writeArrayFieldStart("peaks");

        for(int i = 0; i < spectrum.size(); ++i) {

            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("mz",spectrum.getMz(i));//
            jsonGenerator.writeNumberField("i",spectrum.getIntensity(i));

            List<NRPFragmentAnnotation> nrpFragmentAnnotations=spectrum.getAnnotations(i);

            //1>libpeak 2>others
            jsonGenerator.writeArrayFieldStart("annotations");
            boolean isIsotope= false;
            for (int a=1; a<nrpFragmentAnnotations.size();a++){
                NRPFragmentAnnotation fragAnnot= nrpFragmentAnnotations.get(a);
                isIsotope=fragAnnot.isIsotope();
                if(isIsotope){
                    break;
                }
                jsonGenerator.writeStartObject();
                    jsonGenerator.writeNumberField("theoreticalMz",fragAnnot.getTheoreticalMz());
                    jsonGenerator.writeNumberField("theoreticalMass",fragAnnot.getTheoreticalMass());
                    jsonGenerator.writeNumberField("charge",fragAnnot.getCharge());
                    jsonGenerator.writeStringField("neutralLosses",fragAnnot.getNeutralLosses());
                    jsonGenerator.writeStringField("composition",fragAnnot.getComposition().toString());
                    jsonGenerator.writeObjectField("annotation", fragAnnot.getFragment().getAnnotation());
                    jsonGenerator.writeArrayFieldStart("monomerNodeIdxs");
                        for(MSFragment msFragment:fragAnnot.getFragment().getAllMonomerNode()) {
                            jsonGenerator.writeNumber(msFragment.getIndex());
                        }
                    jsonGenerator.writeEndArray();
                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeBooleanField("isIsotope",isIsotope);
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();

    }

}
