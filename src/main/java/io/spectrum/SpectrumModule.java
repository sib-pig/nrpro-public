package io.spectrum;

import com.fasterxml.jackson.core.util.VersionUtil;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;

public class SpectrumModule extends SimpleModule {
    private static final String NAME = "CustomAtomicGraphModul";
    private static final VersionUtil VERSION_UTIL = new VersionUtil() {
    };

    public SpectrumModule() {
        super("CustomAtomicGraphModul", VERSION_UTIL.version());
        this.addSerializer(ConsensusSpectrum.class, new ConsensusSpectrumSerializer());
    }
}
