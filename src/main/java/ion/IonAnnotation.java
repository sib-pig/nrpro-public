package ion;

public enum IonAnnotation {

    X_A,
    A_A,
    Y_A,
    B_A,
    Z_A,
    C_A,
    X_E,
    A_E,
    Y_E,
    B_E,
    Z_E,
    C_E,
    X_T,
    A_T,
    Y_T,
    B_T,
    Z_T,
    C_T,
    Y_G,
    B_G,
    Z_G,
    C_G
}

