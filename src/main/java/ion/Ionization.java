package ion;

import molecules.bond.BondType;

import java.util.Set;

public class Ionization {
    private DissociationTechnique dissociationTechnique;
    private Set<BondType> bondTypes;

    public Ionization() {
    }

    public DissociationTechnique getDissociationTechnique() {
        return dissociationTechnique;
    }

    public Set<BondType> getBondTypes() {
        return bondTypes;
    }
}

