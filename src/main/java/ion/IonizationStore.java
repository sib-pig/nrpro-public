package ion;

import com.fasterxml.jackson.databind.JsonNode;
import io.JsonReaderNRPro;
import molecules.bond.BondType;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class IonizationStore {
    private static IonizationStore ourInstance = new IonizationStore();
    private Map<DissociationTechnique, Set<BondType>> ionizations= new HashMap<>();
    private String ionsResource =System.getProperty("dissociationTech.store.resource", "dissociationTechIons.json");
    private JsonReaderNRPro jsonReaderNRPro = new JsonReaderNRPro();


    public static IonizationStore getInstance() {

        return ourInstance;
    }

    private IonizationStore() {
        try {
            mapResources();
        }catch (IOException e){
            throw new IllegalStateException("The dissociation techniques resource file could not be loaded", e);
        }
    }


    private void mapResources() throws IOException {
        InputStream fileResidues = this.getClass().getResourceAsStream(ionsResource);
        JsonNode residuesNode = jsonReaderNRPro.readFile(fileResidues);
        addTechnique(residuesNode);
    }

    private void addTechnique(JsonNode residuesNode) throws IOException {

        List<Ionization> listTechniques = jsonReaderNRPro.jsonNode2list(residuesNode, Ionization.class);
        for (Ionization ionization : listTechniques) {
            this.ionizations.put( ionization.getDissociationTechnique(), ionization.getBondTypes());
        }
    }

    public Set<BondType> getBondsIonitzation(DissociationTechnique dissociationTechnique){
        return this.ionizations.get(dissociationTechnique);
    }




}
