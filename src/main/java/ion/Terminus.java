package ion;

import bond.MSBondType;

/**
 * Created by ericart on 06/12/2016.
 */
public class Terminus {

    private boolean hGain;
    private boolean protonAcceptor;
    private IonAnnotation ionAnnotation;
    private IonType ionType;
    private MSBondType msBondType;
    private int[] bondPosition;

    public Terminus() { }

    public Terminus(Terminus terminus, int [] bondPosition){
        this.hGain=terminus.ishGain();
        this.protonAcceptor=terminus.isProtonAcceptor();
        this.ionAnnotation=terminus.getIonAnnotation();
        this.ionType=terminus.getIonType();
        this.msBondType=terminus.getMsBondType();
        this.bondPosition=bondPosition;

    }
    public boolean ishGain() {
        return hGain;
    }

    public boolean isProtonAcceptor() {
        return protonAcceptor;
    }

    public IonAnnotation getIonAnnotation() {
        return ionAnnotation;
    }

    public IonType getIonType() {
        return ionType;
    }

    public MSBondType getMsBondType() {
        return msBondType;
    }

    public int[] getBondPosition() {
        return bondPosition;
    }
}
