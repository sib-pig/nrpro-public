package ion;

public class Tolerance {

    private double tolerance;
    private UnitTolerance unitTolerance;

    public Tolerance(double tolerance, UnitTolerance unitTolerance) {
        this.tolerance = tolerance;
        this.unitTolerance = unitTolerance;
    }

    public enum UnitTolerance {

        DA,
        PPM
    }

    public double getTolerance() {
        return tolerance;
    }

    public UnitTolerance getUnitTolerance() {
        return unitTolerance;
    }
}
