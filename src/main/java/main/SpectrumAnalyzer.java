package main;

import analysis.Scores;
import analysis.SpectrumMatcher;
import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import db.MongoConnection;
import decoy.DecoySpectrumMatcherHelper;
import io.NRProCompoundMapper;
import db.NRProCompound;
import decoy.DecoyTree;
import ion.Tolerance;
import misc.Adduct;
import misc.Candidate;
import misc.IntensityNormalizer;
import misc.Ppm2DaConverter;
import org.apache.commons.math3.distribution.WeibullDistribution;
import org.bson.Document;
import org.expasy.mzjava.core.mol.PeriodicTable;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.peakfilter.AbstractMergePeakFilter;
import org.expasy.mzjava.core.ms.peaklist.peaktransformer.IdentityPeakProcessor;
import org.expasy.mzjava.core.ms.spectrum.MsnSpectrum;
import stats.DistributionCalculator;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

public class SpectrumAnalyzer {

    private MongoCollection structuresCollection;
    private MongoCollection decoyTreeCollection;
    private SpectrumMatcher spectralMatcher;
    private Tolerance ionTolerance;
    private double parentTolerance;
    private boolean useDecoy;
    private NRProCompoundMapper mapper;
    private Adduct protonAdduct;
    private boolean isParentTolInPpm =false;
    private double consensusTolerance;

    public SpectrumAnalyzer(SpectrumMatcher spectralMatcher, NRProCompoundMapper mapper , Tolerance parentTolerance,  boolean useDecoy) {
        this.spectralMatcher = spectralMatcher;
        this.ionTolerance= spectralMatcher.getIonTolerance();
        this.parentTolerance =parentTolerance.getTolerance();
        this.useDecoy=useDecoy;
        this.mapper=mapper;
        this.protonAdduct=spectralMatcher.getProtonAdduct();
        if(parentTolerance.getUnitTolerance()==Tolerance.UnitTolerance.PPM){
            isParentTolInPpm =true;
        }
        String decoyCollectName="decoy_"+spectralMatcher.getDissociationTechnique().toString()+"_"+this.protonAdduct.toString();
        System.out.println("decoy used: "+decoyCollectName);
        //********************mongo db connection********************//
        MongoConnection mongoConnection=MongoConnection.getInstance() ;
        this.structuresCollection = mongoConnection.getCollection("structures");
        this.decoyTreeCollection = mongoConnection.getCollection(decoyCollectName);

        if(ionTolerance.getUnitTolerance()==Tolerance.UnitTolerance.PPM){
            this.consensusTolerance=0.01;
        }else{
            this.consensusTolerance=ionTolerance.getTolerance();
        }
    }

    public List<Candidate> analyzeSpectra(String fileName, List<MsnSpectrum> listSpectrum) throws IOException, URISyntaxException {

        ConsensusSpectrum consensusSpectrum = ConsensusSpectrum.Builder.getBuilder()
                .spectra(listSpectrum)
                .setPeakFilterParams(0.5, 25)
                .fragMzTolerance(consensusTolerance)
                .intensityCombMethod(AbstractMergePeakFilter.IntensityMode.MEAN_ALL_INTENSITY).
                        build();

        double precursorMz =  consensusSpectrum.getPrecursor().getMz();
        int charge=   consensusSpectrum.getPrecursor().getCharge();

        if(charge == 0 || charge>2){
            System.out.println("Peptide charge not specified or too high (triple charged not accepted)");
            return Collections.emptyList();
        }

        IntensityNormalizer.normalizeSpectrumInt(consensusSpectrum);
        double massCharge=(protonAdduct.getModMass()*charge)-PeriodicTable.ELECTRON_MASS*charge;
        double mass=(precursorMz*charge)-massCharge;
        double tolerance= parentTolerance;
        if(this.isParentTolInPpm){
            tolerance= Ppm2DaConverter.convert(this.parentTolerance,mass);
        }

        //********************candidates scoring********************//
        List<Candidate> candidates= new ArrayList<>();
        BasicDBObject gtQuery = new BasicDBObject();
        gtQuery.put("monoisotopicMass", (new BasicDBObject("$gt", mass-tolerance)).append("$lt", mass+tolerance));
        FindIterable findIterable = structuresCollection.find(gtQuery);
        MongoCursor mongoCursor=findIterable.iterator();

        boolean hasNonZeroCandidates=false;
        while (mongoCursor.hasNext()){
            ConsensusSpectrum consensusSpectrumCandidate= consensusSpectrum.copy(new IdentityPeakProcessor<>());
            Object o = mongoCursor.next();
            BasicDBObject obj = new BasicDBObject();
            obj.put("peptide", o);
            String output = mapper.readTree(obj.toString()).path("peptide").toString();
            NRProCompound nrProCompound=mapper.readValue(output, NRProCompound.class);
            //System.out.println("******************************************************************************");
            //System.out.println(nrProCompound.getName());
            Scores scores=spectralMatcher.getSpectralMatch(nrProCompound,consensusSpectrumCandidate,charge,false);
            candidates.add(new Candidate(nrProCompound,scores,new URI(fileName),consensusSpectrumCandidate));
            if(scores.getDotProduct()!=0){
                hasNonZeroCandidates=true;
            }
        }
        mongoCursor.close();
        System.out.println("*********************************************");
        System.out.println(fileName);
        if(useDecoy && hasNonZeroCandidates){
            //********************decoy database scoring********************//
            BasicDBObject gtQuery2 = new BasicDBObject();
            gtQuery2.put("precursorMass", (new BasicDBObject("$gt", mass-tolerance)).append("$lt", mass+tolerance+40));
            FindIterable findIterable2 = decoyTreeCollection.find(gtQuery2);
            MongoCursor<Document> mongoCursor2=findIterable2.iterator();

            //Do a map of dot products in case there is ovelap to not calculate it again
            List<Double> dotProducts= new ArrayList<>();
            Set<Double> dotProductsSet= new HashSet<>();
            while (mongoCursor2.hasNext()){
                Document o  = mongoCursor2.next();
                BasicDBObject obj = new BasicDBObject();
                obj.put("decoyTree", o);
                String output = mapper.readTree(obj.toString()).path("decoyTree").toString();
                DecoyTree decoyTree=mapper.readValue(output, DecoyTree.class);
                //String id=decoyCompound.getOriginalCompId();
                DecoySpectrumMatcherHelper decoySpectrumMatcherHelper = new DecoySpectrumMatcherHelper(ionTolerance);
                double dotproduct= decoySpectrumMatcherHelper.match(decoyTree,consensusSpectrum,charge,spectralMatcher.getNeutralLosses(), spectralMatcher.getAdducts());
                if(dotproduct!=0){
                    dotProducts.add(dotproduct);
                    dotProductsSet.add(dotproduct);
                }
            }
            mongoCursor2.close();

            /*for(double d: dotProducts){
                System.out.println(d);
            }*/
            if(dotProductsSet.size()>=5){
                //if we use gamma distribution:
                //GammaDistribution distribution= DistributionCalculator.getGammaDist(dotProducts);
                //if we use weibull distribution:
                //LogNormalDistribution distribution = DistributionCalculator.getLogNormalDist(dotProducts);
                WeibullDistribution distribution = DistributionCalculator.getWeibullDist(dotProducts);

                for(Candidate candidate:candidates){
                    double cumProb=distribution.cumulativeProbability(candidate.getDotproduct());
                    if(candidate.getScoredPeaks()>3){
                        if(cumProb<1){
                            candidate.setPvalue(1-cumProb);
                        }else{
                            candidate.setPvalue(1E-16);
                        }
                    }
                }
            }
            System.out.println("Dot products size: "+dotProductsSet.size());
            System.gc();
        }

        for(Candidate candidate:candidates){
            System.out.println("name: "+ candidate.getNrp().getName());
            System.out.println("dotProduct: "+candidate.getDotproduct());
            System.out.println("pvalue: "+candidate.getPvalue());
        }
        System.out.println("*********************************************");
        return candidates;
    }
}
