package misc;

import org.expasy.mzjava.core.mol.PeriodicTable;

public enum Adduct{

    H2O (18.010564686),
    CO(27.994914622),
    NH3(17.026549101),
    NH(15.015),
    CH3(15.035),
    K (PeriodicTable.K_MASS),
    Na (PeriodicTable.Na_MASS),
    H (PeriodicTable.H_MASS);


    private double aMass;

    Adduct (double aMass) {
        this.aMass = aMass;
    }

    public double getModMass() {
        return aMass;
    }

}
