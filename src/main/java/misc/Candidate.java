package misc;

import analysis.Scores;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.misc.CandidateSerializer;
import db.NRProCompound;
import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;

import java.net.URI;

@JsonSerialize(using = CandidateSerializer.class)
public class Candidate {

    private NRProCompound nrp;
    private double dotproduct;
    private double pvalue=1;
    private double score=0;
    private int annotPeaks;
    private int scoredPeaks;
    private int isotopePeaks;
    private int peaksHigher5;
    private URI spectrumSource;
    private ConsensusSpectrum consensusSpectrum;


    public Candidate(NRProCompound nrp, Scores scores, URI spectrumSource, ConsensusSpectrum consensusSpectrum) {

        this.nrp = nrp;
        this.dotproduct = scores.getDotProduct();
        this.annotPeaks= scores.getNumAnnotPeaks();
        this.scoredPeaks= scores.getNumScoredPeaks();
        this.isotopePeaks=scores.getNumIsotopes();
        this.peaksHigher5 =scores.getPeaksHigher10();
        this.spectrumSource = spectrumSource;
        this.consensusSpectrum = consensusSpectrum;
    }


    public NRProCompound getNrp() {
        return nrp;
    }

    public double getDotproduct() {
        return dotproduct;
    }

    public double getPvalue() {
        return pvalue;
    }

    public double getScore() {
        return score;
    }

    public URI getSpectrumSource() {
        return spectrumSource;
    }

    public ConsensusSpectrum getConsensusSpectrum() {
        return consensusSpectrum;
    }

    public int getAnnotPeaks() {
        return annotPeaks;
    }

    public int getScoredPeaks() {
        return scoredPeaks;
    }

    public int getIsotopePeaks() {
        return isotopePeaks;
    }

    public int getPeaksHigher5() {
        return peaksHigher5;
    }


    public void setPvalue(double pvalue) {
        this.pvalue = pvalue;
        this.score=-10*Math.log10(pvalue);
       // this.score =
    }



}
