package misc;

import interfaces.Formulable;
import org.expasy.mzjava.core.mol.Composition;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by ericart on 02.02.2016.
 */

public class CompositionsJoiner {

    public static <T extends Formulable> Composition joinCompositions(Collection<T> monomerGraph){
        List<Composition> listCompositions = new ArrayList<>();

        for (T monomer : monomerGraph)
            listCompositions.add( monomer.getComposition());

        return (new Composition(listCompositions.toArray(new Composition[listCompositions.size()])));
    }
}
