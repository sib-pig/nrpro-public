package misc;

import org.expasy.mzjava.core.ms.consensus.ConsensusSpectrum;
import org.expasy.mzjava.core.ms.peaklist.PeakList;

import java.util.Arrays;

public class IntensityNormalizer {
    public static void normalizeSpectrumInt(ConsensusSpectrum consensusSpectrum){
        double[] intensities=consensusSpectrum.getIntensities(new double[consensusSpectrum.size()]);
        double maxInt= Arrays.stream(intensities).max().getAsDouble();
        for(int i=0;i<intensities.length;i++){
            double intNormalized=(intensities[i]/maxInt)*100;
            consensusSpectrum.setIntensityAt(intNormalized,i);
        }
    }
    public static ConsensusSpectrum normalizeIntAndFilter(ConsensusSpectrum consensusSpectrum, double minInt){
        double[] intensities=consensusSpectrum.getIntensities(new double[consensusSpectrum.size()]);
        double maxInt= Arrays.stream(intensities).max().getAsDouble();
        ConsensusSpectrum filteredConsensus = new ConsensusSpectrum<>(consensusSpectrum.size(), PeakList.Precision.DOUBLE,consensusSpectrum.getMemberIds());
        for(int i=0;i<intensities.length;i++){
            double intNormalized=(intensities[i]/maxInt)*100;
            if(intNormalized>=minInt){
                filteredConsensus.add(consensusSpectrum.getMz(i),intNormalized);
            }
        }

        return filteredConsensus;
    }
}
