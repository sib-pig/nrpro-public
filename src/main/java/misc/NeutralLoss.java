package misc;

public enum NeutralLoss {

    H2O (18.010564686),
    NH3 (17.026549101),
    C2H4 (28.031300128);

    private double nlMass;

    NeutralLoss(double nlMass) {
        this.nlMass = nlMass;
    }

    public double getModMass() {
        return nlMass;
    }

}
