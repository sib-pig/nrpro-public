package misc;

public class Ppm2DaConverter {
    public static double convert(double ppmTolerance, double experimentalMz ){
        double  maxMz= ((experimentalMz*ppmTolerance)-(experimentalMz*1.0e6))/(-1.0e6);
        return Math.round((experimentalMz-maxMz)*1.0e6)/1.0e6;
    }
}
