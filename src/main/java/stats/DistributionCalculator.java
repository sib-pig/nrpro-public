package stats;

import org.apache.commons.math3.distribution.GammaDistribution;
import org.apache.commons.math3.distribution.LogNormalDistribution;
import org.apache.commons.math3.distribution.WeibullDistribution;

import java.util.List;

public class DistributionCalculator {


    public static GammaDistribution getGammaDist(List<Double> decoyDotProducts){

        double[] values=decoyDotProducts.stream().mapToDouble(d->d).toArray();
        GammaDist gammaDist= GammaDist.getInstanceFromMLE(values,values.length);
        return new GammaDistribution(gammaDist.getAlpha(),1/gammaDist.getLambda());


    }

    public static WeibullDistribution getWeibullDist (List<Double> decoyDotProducts){

        double[] values=decoyDotProducts.stream().mapToDouble(d->d).toArray();
        WeibullDist weibullDist = WeibullDist.getInstanceFromMLE(values,values.length);
        return new WeibullDistribution(weibullDist.getAlpha(),1/weibullDist.getLambda());
    }

    public static LogNormalDistribution getLogNormalDist(List<Double> decoyDotProducts){

        double[] values=decoyDotProducts.stream().mapToDouble(d->d).toArray();
        LognormalDist lognormalDist = LognormalDist.getInstanceFromMLE(values,values.length);
        return new LogNormalDistribution(lognormalDist.getMu(),lognormalDist.getSigma());
    }
}
